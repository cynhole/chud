local CHUD = CHUD

local IsValid = IsValid
local Lerp = Lerp
local RealFrameTime = RealFrameTime
local ColorAlpha = ColorAlpha
local ScreenScaleH = ScreenScaleH

local draw_RoundedBox = draw.RoundedBox
local draw_DrawText = draw.DrawText

local game_GetAmmoName = game.GetAmmoName

local render_SetScissorRect = render.SetScissorRect

local math_floor = math.floor
local math_max = math.max
local math_Round = math.Round

local rft = RealFrameTime()
local lply = LocalPlayer()
local wep = NULL
local oldwep = NULL
local veh = NULL
local vehWep = false

local colText = CHUD:GetHUDColor()
local colDamage = CHUD:GetDamageColor()
local colBack = CHUD:GetBackgroundColor(false)
local colBackAlpha = CHUD:GetBackgroundColor()

local icons = CHUD:GetBool("HUDIcons")
local iconOffset = 0

local ammoChars = {
    ["357"] = "q",
    ["alyxgun"] = "p",
    ["ar2"] = "u",
    ["ar2altfire"] = "z",
    ["xbowbolt"] = "w",
    ["grenade"] = "v",
    ["pistol"] = "p",
    ["buckshot"] = "s",
    ["rpg_round"] = "x",
    ["smg1"] = "r",
    ["smg1_grenade"] = "t",
}

local ammoCharsWeps = {
    ["weapon_annabelle"] = "s",
    ["weapon_annabeller"] = "s"
}

local hudChars = {
    ["health"] = "+",
    ["armor"] = "*",
    ["velocity"] = "D",
}

local h_old = 0
local h_glowa = 255
local h_flash = 1
local h_w = ScreenScaleH(102)
local h_c = 0
local function DrawHealth(x, y)
    local hp = lply:Health()
    local hpMax = lply:GetMaxHealth()

    local c = 1
    if hp / hpMax <= 0.25 then c = 0 end
    h_c = Lerp(rft * 4, h_c, c)

    if hp / hpMax <= 0.25 then
        h_flash = Lerp(rft / 0.8, h_flash, 0)
        if math_floor(h_flash + 0.5) == 0 then
            h_flash = 1
        end
    else
        h_flash = Lerp(rft * 4, h_flash, 0)
    end

    local col = CHUD:IntersectColor(colDamage, colText, h_c)
    local bgcol = ColorAlpha(CHUD:IntersectColor(colBack, colDamage, h_flash), CHUD:GetInt("BackgroundAlpha"))

    if hp ~= h_old then
        h_old = hp
        h_glowa = 255
    end

    h_glowa = Lerp(rft * 1.5, h_glowa, 0)

    local nw = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", math_max(100, hp))

    h_w = Lerp(rft / 0.1, h_w, ScreenScaleH(102) + (nw - CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "100")))

    render_SetScissorRect(x, y, x + h_w, y + ScreenScaleH(36), true)

    draw_RoundedBox(10, x, y, h_w, ScreenScaleH(36), bgcol)

    draw_DrawText("HEALTH", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20) + iconOffset, col, TEXT_ALIGN_LEFT)

    if icons then
        draw_DrawText(hudChars["health"], "CHUD-HL2-WeaponIconsSmall2", x + ScreenScaleH(10), y - ScreenScaleH(15), col, TEXT_ALIGN_LEFT)
    end

    local hpClamped = CHUD:GetBool("ClampNegative") and hp or math_max(0, hp)
    draw_DrawText(hpClamped, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(50), y + ScreenScaleH(2), ColorAlpha(col, h_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(hpClamped, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(50), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)

    render_SetScissorRect(0, 0, 0, 0, false)
end

local a_old = 0
local a_glowa = 255
local a_w = ScreenScaleH(108)
local function DrawArmor(x, y)
    local col = colText

    local armor = lply:Armor()
    local maxArmor = lply:GetMaxArmor()

    if armor ~= a_old then
        a_old = armor
        a_glowa = 255
    end

    a_glowa = Lerp(rft * 1.5, a_glowa, 0)

    local nw = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", math_max(100, armor))

    a_w = Lerp(rft / 0.1, a_w, ScreenScaleH(108) + (nw - CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "100")))

    render_SetScissorRect(x, y, x + a_w, y + ScreenScaleH(36), true)

    draw_RoundedBox(10, x, y, a_w, ScreenScaleH(36), colBackAlpha)

    draw_DrawText("SUIT", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20) + iconOffset, col, TEXT_ALIGN_LEFT)

    if icons then
        draw_DrawText(hudChars["armor"], "CHUD-HL2-WeaponIconsSmall2", x + ScreenScaleH(10), y - ScreenScaleH(15), col, TEXT_ALIGN_LEFT)
    end

    draw_DrawText(armor, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(50), y + ScreenScaleH(2), ColorAlpha(col, a_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(armor, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(50), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)

    render_SetScissorRect(0, 0, 0, 0, false)
end

local am_old = 0
local am_glowa = 255
local am_bga = CHUD:GetInt("BackgroundAlpha")
local am_w = ScreenScaleH(132)
local am_flash = 0
local am_c = 0
local function DrawAmmo(x, y)
    if IsValid(veh) and not vehWep then return end

    local clip = wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()

    local c = 1
    if clip == 0 then c = 0 end
    am_c = Lerp(rft * 4, am_c, c)

    if clip == 0 then
        am_flash = Lerp(rft / 0.8, am_flash, 0)
        if math_floor(am_flash + 0.5) == 0 then
            am_flash = 1
        end
    else
        am_flash = Lerp(rft * 4, am_flash, 0)
    end

    local col = CHUD:IntersectColor(colDamage, colText, am_c)
    local bgcol = ColorAlpha(CHUD:IntersectColor(colBack, colDamage, am_flash), CHUD:GetInt("BackgroundAlpha"))

    local w1 = CHUD:GetTextSize("CHUD-HL2-HUDDefault", "AMMO")
    local w2 = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "0000")
    local w3 = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "000")
    local w4 = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", wep:Clip1())

    if clip == 0 then
        am_glowa = 255
    end

    if clip ~= am_old then
        am_old = clip
        am_glowa = 255
    end

    am_glowa = Lerp(rft * 1.5, am_glowa, 0)

    local targetW = math.max(ScreenScaleH(132) - w3 + w4, ScreenScaleH(132))
    local longWidth = lply:GetAmmoCount(wep:GetPrimaryAmmoType()) > 999 and ScreenScaleH(8) or 0
    if wep:GetPrimaryAmmoType() == -1 or wep:Clip1() == -1 then
        targetW = ScreenScaleH(16) + w1 + w2 + longWidth
    end

    am_w = Lerp(rft / 0.1, am_w, targetW)

    render_SetScissorRect(x, y, x + am_w, y + ScreenScaleH(36), true)

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), bgcol)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), ColorAlpha(col, am_bga))

    draw_DrawText("AMMO", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)

    if (ammoCharsWeps[wep:GetClass()] or ammoChars[game_GetAmmoName(wep:GetPrimaryAmmoType()):lower()]) and CHUD:GetBool("AmmoIcons") then
        local nLabelWidth, nLabelHeight = CHUD:GetTextSize("CHUD-HL2-HUDDefault", "AMMO")
        local iw, ih = CHUD:GetTextSize("CHUD-HL2-WeaponIconsSmall", ammoCharsWeps[wep:GetClass()] or ammoChars[game_GetAmmoName(wep:GetPrimaryAmmoType()):lower()] or "")
        local ix = ScreenScaleH(8) + (nLabelWidth - iw) / 2
        local iy = ScreenScaleH(20) - (nLabelHeight + (ih / 2))

        draw_DrawText(ammoCharsWeps[wep:GetClass()] or ammoChars[game_GetAmmoName(wep:GetPrimaryAmmoType()):lower()] or "", "CHUD-HL2-WeaponIconsSmall", x + ix, y + iy, col, TEXT_ALIGN_LEFT)
    end

    draw_DrawText(clip, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(44), y + ScreenScaleH(2), ColorAlpha(col, am_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clip, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(44), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)

    if wep:GetPrimaryAmmoType() ~= -1 and wep:Clip1() ~= -1 then
        draw_DrawText(lply:GetAmmoCount(wep:GetPrimaryAmmoType()), "CHUD-HL2-HUDNumbersSmall", x + am_w - ScreenScaleH(34), y + ScreenScaleH(16), col, TEXT_ALIGN_LEFT)
    end

    render_SetScissorRect(0, 0, 0, 0, false)
end

local function DrawAmmoCustom(x, y)
    if IsValid(veh) and not vehWep then return end

    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.PrimaryAmmo and not tbl.PrimaryClip then return end

    local clip = tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)

    local c = 1
    if clip == 0 then c = 0 end
    am_c = Lerp(rft * 4, am_c, c)

    if clip == 0 then
        am_flash = Lerp(rft / 0.8, am_flash, 0)
        if math_floor(am_flash + 0.5) == 0 then
            am_flash = 1
        end
    else
        am_flash = Lerp(rft * 4, am_flash, 0)
    end

    local col = CHUD:IntersectColor(colDamage, colText, am_c)
    local bgcol = ColorAlpha(CHUD:IntersectColor(colBack, colDamage, am_flash), CHUD:GetInt("BackgroundAlpha"))

    local w1 = CHUD:GetTextSize("CHUD-HL2-HUDDefault", "AMMO")
    local w2 = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "0000")

    if clip == 0 then
        am_glowa = 255
    end

    if clip ~= am_old then
        am_old = clip
        am_glowa = 255
    end

    am_glowa = Lerp(rft * 1.5, am_glowa, 0)
    am_w = Lerp(rft / 0.1, am_w, (not tbl.PrimaryAmmo or not tbl.PrimaryClip) and ScreenScaleH(24) + w1 + w2 or ScreenScaleH(136))

    render_SetScissorRect(x, y, x + am_w, y + ScreenScaleH(36), true)

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), bgcol)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), ColorAlpha(col, am_bga))

    draw_DrawText("AMMO", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)

    draw_DrawText(clip, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(44), y + ScreenScaleH(2), ColorAlpha(col, am_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clip, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(44), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)

    if tbl.PrimaryAmmo and tbl.PrimaryClip then
        draw_DrawText(tbl.PrimaryAmmo or 0, "CHUD-HL2-HUDNumbersSmall", x + ScreenScaleH(98), y + ScreenScaleH(16), col, TEXT_ALIGN_LEFT)
    end

    render_SetScissorRect(0, 0, 0, 0, false)
end

local am2_old = 0
local am2_glowa = 255
local am2_w = ScreenScaleH(46)
local am2_c = 0
local function DrawAmmoAlt(x, y)
    if IsValid(veh) and not vehWep then return end

    local c = 1
    if lply:GetAmmoCount(wep:GetSecondaryAmmoType()) == 0 then c = 0 end
    am2_c = Lerp(rft * 4, am2_c, c)
    local col = CHUD:IntersectColor(colDamage, colText, am2_c)

    local nw = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", lply:GetAmmoCount(wep:GetSecondaryAmmoType()))

    if lply:GetAmmoCount(wep:GetSecondaryAmmoType()) ~= am2_old then
        am2_old = lply:GetAmmoCount(wep:GetSecondaryAmmoType())
        am2_glowa = 255
    end

    am2_glowa = Lerp(rft * 1.5, am2_glowa, 0)
    am2_w = Lerp(rft / 0.1, am2_w, ScreenScaleH(46) + nw)

    render_SetScissorRect(x, y, x + am2_w, y + ScreenScaleH(36), true)
    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), colBackAlpha)
    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), ColorAlpha(col, am_bga))

    draw_DrawText("ALT", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(22), col, TEXT_ALIGN_LEFT)

    if ammoChars[game_GetAmmoName(wep:GetSecondaryAmmoType()):lower()] and CHUD:GetBool("AmmoIconsSecondary") then
        local nLabelWidth, nLabelHeight = CHUD:GetTextSize("CHUD-HL2-HUDDefault", "ALT")
        local iw, ih = CHUD:GetTextSize("CHUD-HL2-WeaponIconsSmall", ammoChars[game_GetAmmoName(wep:GetSecondaryAmmoType()):lower()] or "")
        local ix = ScreenScaleH(8) + (nLabelWidth - iw) / 2
        local iy = ScreenScaleH(22) - (nLabelHeight + (ih / 2))

        draw_DrawText(ammoChars[game_GetAmmoName(wep:GetSecondaryAmmoType()):lower()] or "", "CHUD-HL2-WeaponIconsSmall", x + ix, y + iy, col, TEXT_ALIGN_LEFT)
    end

    draw_DrawText(lply:GetAmmoCount(wep:GetSecondaryAmmoType()), "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(36), y + ScreenScaleH(2), ColorAlpha(col, am2_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(lply:GetAmmoCount(wep:GetSecondaryAmmoType()), "CHUD-HL2-HUDNumbers", x + ScreenScaleH(36), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
    render_SetScissorRect(0, 0, 0, 0, false)
end

local function DrawAmmoAltCustom(x, y)
    if IsValid(veh) and not vehWep then return end

    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.SecondaryAmmo then return end

    local clip = tbl.SecondaryAmmo or 0

    local c = 1
    if clip == 0 then c = 0 end
    am2_c = Lerp(rft * 4, am2_c, c)
    local col = CHUD:IntersectColor(colDamage, colText, am2_c)

    local nw = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", clip)

    if clip ~= am2_old then
        am2_old = clip
        am2_glowa = 255
    end

    am2_glowa = Lerp(rft * 1.5, am2_glowa, 0)
    am2_w = Lerp(rft / 0.1, am2_w, ScreenScaleH(48) + nw)

    render_SetScissorRect(x, y, x + am2_w, y + ScreenScaleH(36), true)

    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), colBackAlpha)
    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(36), ColorAlpha(col, am_bga))

    draw_DrawText("ALT", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(22), col, TEXT_ALIGN_LEFT)

    draw_DrawText(clip, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(36), y + ScreenScaleH(2), ColorAlpha(col, am2_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clip, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(36), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)

    render_SetScissorRect(0, 0, 0, 0, false)
end

local function DrawVehicleAmmo(x, y)
    local _, clip, ammo = veh:GetAmmo()

    if ammo == -1 then return end

    local clipNum = clip == -1 and ammo or clip

    local c = 1
    if clipNum == 0 then c = 0 end
    am_c = Lerp(rft * 4, am_c, c)

    if clipNum == 0 then
        am_flash = Lerp(rft / 0.8, am_flash, 0)
        if math_floor(am_flash + 0.5) == 0 then
            am_flash = 1
        end
    else
        am_flash = Lerp(rft * 4, am_flash, 0)
    end

    local col = CHUD:IntersectColor(colDamage, colText, am_c)
    local bgcol = ColorAlpha(CHUD:IntersectColor(colBack, colDamage, am_flash), CHUD:GetInt("BackgroundAlpha"))

    local w1 = CHUD:GetTextSize("CHUD-HL2-HUDDefault", "AMMO")
    local w2 = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "0000")

    if clipNum == 0 then
        am_glowa = 255
    end

    if clipNum ~= am_old then
        am_old = clipNum
        am_glowa = 255
    end

    am_glowa = Lerp(rft * 1.5, am_glowa, 0)
    am_w = Lerp(rft / 0.1, am_w, clip == -1 and ScreenScaleH(24) + w1 + w2 or ScreenScaleH(136))

    render_SetScissorRect(x, y, x + am_w, y + ScreenScaleH(36), true)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), bgcol)
    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(36), ColorAlpha(col, am_bga))

    draw_DrawText("AMMO", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20), col, TEXT_ALIGN_LEFT)

    draw_DrawText(clipNum, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(44), y + ScreenScaleH(2), ColorAlpha(col, am_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(clipNum, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(44), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)

    if clip ~= -1 then
        draw_DrawText(ammo, "CHUD-HL2-HUDNumbersSmall", x + ScreenScaleH(98), y + ScreenScaleH(16), col, TEXT_ALIGN_LEFT)
    end

    render_SetScissorRect(0, 0, 0, 0, false)
end

local v_w = ScreenScaleH(102)
local function DrawVelocity(x, y)
    local col = colText

    local v = math_Round(CHUD:GetPlayerVelocity():Length())
    local nw = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", math_max(100, v))

    local vw = ScreenScaleH(102) + nw - CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "100") - CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "0")
    v_w = Lerp(rft / 0.1, v_w, math_max(ScreenScaleH(102), vw))

    render_SetScissorRect(x, y, x + v_w, y + ScreenScaleH(36), true)

    draw_RoundedBox(10, x, y, v_w, ScreenScaleH(36), colBackAlpha)

    draw_DrawText("UPS", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20) + iconOffset, col, TEXT_ALIGN_LEFT)

    if icons then
        draw_DrawText(hudChars["velocity"], "CHUD-HL2-WeaponIconsSmall2", x + ScreenScaleH(10), y - ScreenScaleH(15), col, TEXT_ALIGN_LEFT)
    end

    draw_DrawText(v, "CHUD-HL2-HUDNumbers", x + v_w - ScreenScaleH(8), y + ScreenScaleH(2), col, TEXT_ALIGN_RIGHT)

    render_SetScissorRect(0, 0, 0, 0, false)
end

local am_pos = CHUD.CurResW - am_w
local am2_pos = ScreenScaleH(432)
local vel_xpos = ScreenScaleH(16)
local vel_ypos = ScreenScaleH(432)
local a_xpos = ScreenScaleH(16)

local function Draw()
    rft = RealFrameTime()

    if not IsValid(lply) then
        lply = LocalPlayer()
    end
    wep = lply:GetActiveWeapon()
    veh = lply:GetVehicle()
    vehWep = lply:GetAllowWeaponsInVehicle()

    colText = CHUD:GetHUDColor()
    colDamage = CHUD:GetDamageColor()
    colBack = CHUD:GetBackgroundColor(false)
    colBackAlpha = CHUD:GetBackgroundColor()

    icons = CHUD:GetBool("HUDIcons")
    iconOffset = icons and ScreenScaleH(4) or 0

    if wep ~= oldwep then
        oldwep = wep
        am_bga = CHUD:GetInt("BackgroundAlpha")
    end

    am_bga = Lerp(rft / 0.5, am_bga, 0)

    local shouldDrawHealth = CHUD:ShouldDrawHealth()
    local shouldDrawArmor = CHUD:ShouldDrawArmor()

    if shouldDrawHealth then
        DrawHealth(ScreenScaleH(16), ScreenScaleH(432))
    end

    if shouldDrawArmor then
        a_xpos = Lerp(rft / 0.5, a_xpos, ScreenScaleH(16) + (shouldDrawHealth and h_w + ScreenScaleH(22) or 0))
        DrawArmor(a_xpos, ScreenScaleH(432))
    end

    if IsValid(wep) then
        local altAmmoOldPos = CHUD:GetBool("AltAmmoOldPos") and ScreenScaleH(432) or ScreenScaleH(386)
        local altAmmoOldPosCustom = CHUD:GetBool("AltAmmoOldPos") and am2_w + ScreenScaleH(12) or -ScreenScaleH(2)

        local customAmmo = wep.CustomAmmoDisplay and wep:CustomAmmoDisplay()

        if customAmmo ~= nil then
            am_pos = Lerp(rft / 0.4, am_pos, CHUD.CurResW - am_w - ScreenScaleH(18) - (customAmmo.SecondaryAmmo and ((customAmmo.SecondaryAmmo == 0 and not CHUD.CVars.HideAltWhenEmpty:GetBool()) or customAmmo.SecondaryAmmo ~= 0) and altAmmoOldPosCustom or 0))
            DrawAmmoCustom(am_pos, ScreenScaleH(432))
        elseif wep:GetPrimaryAmmoType() ~= -1 then
            am_pos = Lerp(rft / 0.4, am_pos, CHUD.CurResW - am_w - ScreenScaleH(18) - (wep:GetSecondaryAmmoType() ~= -1 and ((lply:GetAmmoCount(wep:GetSecondaryAmmoType()) == 0 and not CHUD.CVars.HideAltWhenEmpty:GetBool()) or lply:GetAmmoCount(wep:GetSecondaryAmmoType()) ~= 0) and altAmmoOldPosCustom or 0))
            DrawAmmo(am_pos, ScreenScaleH(432))
        end

        if customAmmo ~= nil and customAmmo.SecondaryAmmo then
            am2_pos = Lerp(rft / 0.4, am2_pos, (customAmmo.PrimaryClip == nil and customAmmo.PrimaryAmmo == nil) and ScreenScaleH(432) or altAmmoOldPos)

            if (customAmmo.SecondaryAmmo == 0 and not CHUD:GetBool("HideAltWhenEmpty")) or customAmmo.SecondaryAmmo ~= 0 then
                DrawAmmoAltCustom(CHUD.CurResW - am2_w - ScreenScaleH(16), am2_pos)
            end
        elseif wep:GetSecondaryAmmoType() ~= -1 then
            am2_pos = Lerp(rft / 0.4, am2_pos, wep:GetPrimaryAmmoType() == -1 and ScreenScaleH(432) or altAmmoOldPos)

            if (lply:GetAmmoCount(wep:GetSecondaryAmmoType()) == 0 and not CHUD.CVars.HideAltWhenEmpty:GetBool()) or lply:GetAmmoCount(wep:GetSecondaryAmmoType()) ~= 0 then
                DrawAmmoAlt(CHUD.CurResW - am2_w - ScreenScaleH(16), am2_pos)
            end
        end
    end

    if IsValid(veh) then
        DrawVehicleAmmo(CHUD.CurResW - am_w - ScreenScaleH(18), ScreenScaleH(432))
    end

    if CHUD:GetBool("ShowVelocity") then
        local aux = (AUX and AUXPOW and lply:GetInfoNum("cl_infinite_aux_power", 1) == 0)

        local targetY = ScreenScaleH(432)
        if aux or CHUD:GetInt("VelocityAltPos") > 0 then
            targetY = ScreenScaleH(432)
        elseif shouldDrawHealth or shouldDrawArmor then
            targetY = ScreenScaleH(386)
        end

        local targetX = ScreenScaleH(16)
        if aux or CHUD:GetInt("VelocityAltPos") == 1 then
            targetX = ScreenScaleH(16)
            targetX = targetX + (shouldDrawHealth and h_w + ScreenScaleH(22) or 0)
            targetX = targetX + (shouldDrawArmor and a_w + ScreenScaleH(22) or 0)
        elseif CHUD:GetInt("VelocityAltPos") > 1 then
            targetX = (CHUD.CurResW / 2) - (v_w / 2)
        end

        vel_ypos = Lerp(rft / 0.4, vel_ypos, targetY)
        vel_xpos = Lerp(rft / 0.4, vel_xpos, targetX)

        DrawVelocity(vel_xpos, vel_ypos)
    end
end

local dh_old = 0
local dh_glowa = 255
local dh_w = ScreenScaleH(102)
local function Demo(amt, x, y)
    rft = RealFrameTime()

    if not IsValid(lply) then
        lply = LocalPlayer()
    end
    wep = lply:GetActiveWeapon()
    veh = lply:GetVehicle()
    vehWep = lply:GetAllowWeaponsInVehicle()

    colText = CHUD:GetHUDColor()
    colDamage = CHUD:GetDamageColor()
    colBack = CHUD:GetBackgroundColor(false)
    colBackAlpha = CHUD:GetBackgroundColor()

    local col = amt / 100 <= 0.25 and colDamage or colText
    local bgcol = ColorAlpha(amt / 100 <= 0.25 and colDamage or colBack, CHUD:GetInt("BackgroundAlpha"))

    if amt ~= dh_old then
        dh_old = amt
        dh_glowa = 255
    end

    dh_glowa = Lerp(rft * 1.5, dh_glowa, 0)

    local nw = CHUD:GetTextSize("CHUD-HL2-HUDNumbers", math_max(100, amt))

    dh_w = Lerp(rft / 0.1, dh_w, CHUD:ScreenScaleH(102) + (nw - CHUD:GetTextSize("CHUD-HL2-HUDNumbers", "100")))

    draw_RoundedBox(10, x, y, h_w, ScreenScaleH(36), bgcol)

    draw_DrawText("HEALTH", "CHUD-HL2-HUDDefault", x + ScreenScaleH(8), y + ScreenScaleH(20) + iconOffset, col, TEXT_ALIGN_LEFT)

    if icons then
        draw_DrawText(hudChars["health"], "CHUD-HL2-WeaponIconsSmall2", x + ScreenScaleH(10), y - ScreenScaleH(15), col, TEXT_ALIGN_LEFT)
    end

    local hpClamped = CHUD:GetBool("ClampNegative") and amt or math_max(0, amt)
    draw_DrawText(hpClamped, "CHUD-HL2-HUDNumbersGlow", x + ScreenScaleH(50), y + ScreenScaleH(2), ColorAlpha(col, dh_glowa), TEXT_ALIGN_LEFT)
    draw_DrawText(hpClamped, "CHUD-HL2-HUDNumbers", x + ScreenScaleH(50), y + ScreenScaleH(2), col, TEXT_ALIGN_LEFT)
end

CHUD.Styles["hl2"] = {
    Name = "Half-Life 2",
    Draw = Draw,
    Demo = Demo,
    HasWeaponSwitcher = true,
    WeaponSwitcher = function(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength)
        CHUD:DrawLegacyWeaponSwitcher(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength, {
            WeaponWidth = 112,
            WeaponHeight = 20,
            WeaponHeightSelected = 80,
            TextYPos = 70,
            BoxGap = 8,
            BoxSize = 32,
            SelectionNumberPosX = 4,
            SelectionNumberPosY = 4,
        })
    end,
}
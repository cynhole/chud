CHUD.Styles["oc"] = {
    Name = "Obsidian Conflict",
    HasWeaponSwitcher = true,
    WeaponSwitcher = function(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength)
        CHUD:DrawLegacyWeaponSwitcher(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength, {
            WeaponWidth = 100,
            WeaponHeight = 25,
            WeaponHeightSelected = 60,
            TextYPos = 4,
            BoxGap = 2,
            BoxSize = 25,
            SelectionNumberPosX = 4,
            SelectionNumberPosY = 4,
            IconOffset = 8,
            HideEmptySlots = true,
        })
    end
}
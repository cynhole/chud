local CHUD = CHUD

local IsValid = IsValid
local Lerp = Lerp
local RealFrameTime = RealFrameTime

local draw_RoundedBox = draw.RoundedBox
local draw_DrawText = draw.DrawText

local math_max = math.max
local math_Round = math.Round

local rft = RealFrameTime()
local lply = LocalPlayer()
local wep = NULL
local oldwep = NULL
local veh = NULL
local vehWep = false

local colText = CHUD:GetHUDColor()
local colDamage = CHUD:GetDamageColor()
local colBack = CHUD:GetBackgroundColor(false)
local colBackAlpha = CHUD:GetBackgroundColor()

local h_w = ScreenScaleH(80)
local function DrawHealth(x, y)
    local hp = lply:Health()
    local col = hp <= 25 and colDamage or colText

    local nw = CHUD:GetTextSize("CHUD-CS-Icons", math_max(100, hp))

    h_w = Lerp(rft, h_w, ScreenScaleH(80) + (nw - CHUD:GetTextSize("CHUD-CS-Icons", "100")))

    draw_RoundedBox(10, x, y, h_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText("b", "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
    draw_DrawText(CHUD:GetBool("ClampNegative") and hp or math_max(0, hp), "CHUD-CS-Icons", x + ScreenScaleH(35), y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
end

local a_w = ScreenScaleH(80)
local function DrawArmor(x, y)
    local armor = lply:Armor()
    local nw = CHUD:GetTextSize("CHUD-CS-Icons", math_max(100, armor))

    a_w = Lerp(0.1, a_w, ScreenScaleH(80) + (nw - CHUD:GetTextSize("CHUD-CS-Icons", "100")))

    draw_RoundedBox(10, x, y, a_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText("a", "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), colText, TEXT_ALIGN_LEFT)
    draw_DrawText(armor, "CHUD-CS-Icons", x + ScreenScaleH(34), y + ScreenScaleH(-4), colText, TEXT_ALIGN_LEFT)
end

local am_w = ScreenScaleH(142)
local function DrawAmmo(x, y)
    local clip = wep:Clip1() == -1 and lply:GetAmmoCount(wep:GetPrimaryAmmoType()) or wep:Clip1()
    local col = clip == 0 and colDamage or colText

    local w1 = CHUD:GetTextSize("CHUD-CS-Icons", "0000")
    local w2 = CHUD:GetTextSize("CHUD-CS-Icons", " | ")

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText(clip, "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
    if wep:GetPrimaryAmmoType() ~= -1 and wep:Clip1() ~= -1 then
        draw_DrawText(lply:GetAmmoCount(wep:GetPrimaryAmmoType()), "CHUD-CS-Icons", x + ScreenScaleH(8) + w1 + w2, y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
        draw_DrawText(" | ", "CHUD-CS-Icons", x + ScreenScaleH(8) + w1, y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
    end
end

local function DrawAmmoCustom(x, y)
    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.PrimaryAmmo and not tbl.PrimaryClip then return end

    local col = (tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0)) == 0 and colDamage or colText

    local w1 = CHUD:GetTextSize("CHUD-CS-Icons", "0000")
    local h2 = CHUD:GetTextSize("CHUD-CS-Icons", " | ")

    draw_RoundedBox(10, x, y, am_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText(tbl.PrimaryClip == nil and (tbl.PrimaryAmmo or 0) or (tbl.PrimaryClip or 0), "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
    if tbl.PrimaryAmmo and tbl.PrimaryClip then
        draw_DrawText(tbl.PrimaryAmmo, "CHUD-CS-Icons", x + ScreenScaleH(8) + w1 + h2, y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
        draw_DrawText(" | ", "CHUD-CS-Icons", x + ScreenScaleH(8) + w1, y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
    end
end

local am2_w = ScreenScaleH(80)
local function DrawAmmoAlt(x, y)
    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText(lply:GetAmmoCount(wep:GetSecondaryAmmoType()), "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), colText, TEXT_ALIGN_LEFT)
end

local function DrawAmmoAltCustom(x, y)
    local tbl = wep:CustomAmmoDisplay()
    if not tbl or (tbl and not tbl.Draw) then return end
    if not tbl.SecondaryAmmo then return end

    draw_RoundedBox(10, x, y, am2_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText(tbl.SecondaryAmmo or 0, "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), colText, TEXT_ALIGN_LEFT)
end

local v_w = ScreenScaleH(98)
local function DrawVelocity(x, y)
    local v = math_Round(CHUD:GetPlayerVelocity():Length())

    draw_RoundedBox(10, x, y, v_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText("»", "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), colText, TEXT_ALIGN_LEFT)
    draw_DrawText(v, "CHUD-CS-Icons", x + ScreenScaleH(34), y + ScreenScaleH(-4), colText, TEXT_ALIGN_LEFT)
end

local am2_pos = ScreenScaleH(394)
local dh_w = ScreenScaleH(80)

local function Draw()
    rft = RealFrameTime()

    if not IsValid(lply) then
        lply = LocalPlayer()
    end
    wep = lply:GetActiveWeapon()
    veh = lply:GetVehicle()
    vehWep = lply:GetAllowWeaponsInVehicle()

    colText = CHUD:GetHUDColor()
    colDamage = CHUD:GetDamageColor()
    colBack = CHUD:GetBackgroundColor(false)
    colBackAlpha = CHUD:GetBackgroundColor()

    if CHUD:ShouldDrawHealth() then
        DrawHealth(ScreenScaleH(8), ScreenScaleH(446))
    end

    if CHUD:ShouldDrawArmor() then
        DrawArmor(ScreenScaleH(148), ScreenScaleH(446))
    end

    if IsValid(wep) then
        local customAmmo = wep.CustomAmmoDisplay and wep:CustomAmmoDisplay()
        if customAmmo ~= nil then
            DrawAmmoCustom(CHUD.CurResW - ScreenScaleH(157), ScreenScaleH(446))
        elseif wep:GetPrimaryAmmoType() ~= -1 then
            DrawAmmo(CHUD.CurResW - ScreenScaleH(157), ScreenScaleH(446))
        end

        if customAmmo ~= nil and customAmmo.SecondaryAmmo then
            am2_pos = Lerp(RealFrameTime() / 0.4, am2_pos, (customAmmo.PrimaryClip == nil and customAmmo.PrimaryAmmo == nil) and ScreenScaleH(446) or ScreenScaleH(394))
            DrawAmmoAltCustom(CHUD.CurResW - ScreenScaleH(95), am2_pos)
        elseif wep:GetSecondaryAmmoType() ~= -1 then
            am2_pos = Lerp(RealFrameTime() / 0.4, am2_pos, wep:GetPrimaryAmmoType() == -1 and ScreenScaleH(446) or ScreenScaleH(394))
            DrawAmmoAlt(CHUD.CurResW - ScreenScaleH(95), am2_pos)
        end
    end

    if CHUD:GetBool("ShowVelocity") then
        DrawVelocity((CHUD.CurResW / 2) - (v_w / 2), ScreenScaleH(446))
    end
end

local function Demo(amt,x,y)
    local col = amt <= 25 and colDamage or colText

    local nw = CHUD:GetTextSize("CHUD-CS-Icons", math_max(100, amt))

    dh_w = Lerp(0.1, dh_w, ScreenScaleH(80) + (nw - CHUD:GetTextSize("CHUD-CS-Icons", "100")))

    draw_RoundedBox(10, x, y, dh_w, ScreenScaleH(25), colBackAlpha)
    draw_DrawText("b", "CHUD-CS-Icons", x + ScreenScaleH(8), y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
    draw_DrawText(CHUD:GetBool("ClampNegative") and amt or math_max(0, amt), "CHUD-CS-Icons", x + ScreenScaleH(35), y + ScreenScaleH(-4), col, TEXT_ALIGN_LEFT)
end

CHUD.Styles["css"] = {
    Name = "Counter-Strike: Source",
    Draw = Draw,
    Demo = Demo
}
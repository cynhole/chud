local vars = {
    ["9xUseHUDColor"] = CreateClientConVar("chud_9x_use_hud_color", "1",  true, false, "Use HUD color for bars in 9x style"),
    ["9xBarColor"]    = CreateClientConVar("chud_9x_bar_color",     "14", true, false, "Color for bars in 9x style when using default scheme"),
    ["9xColorScheme"] = CreateClientConVar("chud_9x_color_scheme",  "1",  true, false, "Overall color scheme in 9x style, 1 - Default, 2 - Brick, 3 - Desert, 4 - Eggplant, 5 - Lilac, 6 - Maple, 7 - Marine, 8 - Plum, 9 - Pumpkin, 10 - Rainy Day, 11 - Rose, 12 - Slate, 13 - Spruce, 14 - Wheat, 15 - Windows XP Classic"),
    ["9xAmmoLeft"]    = CreateClientConVar("chud_9x_ammo_left",     "1",  true, false, "Put ammo on left side with armor/health in 9x style"),
}

for name, cvar in pairs(vars) do
    CHUD.CVars[name] = cvar
end

local palette = {
    Color(255, 255, 255),
    Color(  0,   0,   0),
    Color(192, 192, 192),
    Color(128, 128, 128),
    Color(255,   0,   0),
    Color(128,   0,   0),
    Color(255, 255,   0),
    Color(128, 128,   0),
    Color(  0, 255,   0),
    Color(  0, 128,   0),
    Color(  0, 255, 255),
    Color(  0, 128, 128),
    Color(  0,   0, 255),
    Color(  0,   0, 128),
    Color(255,   0, 255),
    Color(128,   0, 128),
}

local schemes = {
    -- default
    {
        palette[1],
        palette[2],
        palette[3],
        palette[4],
    },

    -- Brick
    {
        Color(225, 224, 210),
        palette[2],
        Color(194, 191, 165),
        Color(141, 137,  97),
        palette[6],
    },

    -- Desert
    {
        Color(234, 230, 221),
        palette[2],
        Color(213, 204, 187),
        Color(162, 141, 104),
        palette[12],
    },

    -- Eggplant
    {
        Color(200, 216, 216),
        palette[2],
        Color(144, 176, 168),
        Color( 88, 128, 120),
        Color( 64,   0,  64),
    },

    -- Lilac
    {
        Color(216, 213, 236),
        palette[2],
        Color(174, 168, 217),
        Color( 90,  78, 177),
        Color( 90,  78, 177),
        palette[4],
    },

    -- Maple
    {
        Color(242, 236, 215),
        palette[2],
        Color(230, 216, 174),
        Color(198, 166,  70),
        palette[6],
    },

    -- Marine
    {
        Color(200, 224, 216),
        palette[2],
        Color(136, 192, 184),
        Color( 72, 144, 136),
        palette[14],
    },

    -- Plum
    {
        Color(216, 208, 200),
        palette[2],
        Color(168, 152, 144),
        Color(128,  96,  88),
        Color( 72,  64,  96),
    },

    -- Pumpkin
    {
        Color(245, 234, 207),
        palette[2],
        Color(236, 213, 157),
        Color(215, 165,  47),
        Color( 66,   0,  66),
    },

    -- Rainy Day
    {
        Color(193, 204, 217),
        palette[2],
        Color(131, 153, 177),
        Color( 79, 101, 125),
        Color( 79, 101, 125),
        palette[4],
    },

    -- Rose
    {
        Color(231, 216, 220),
        palette[2],
        Color(207, 175, 183),
        Color(159,  96, 112),
        Color(159,  96, 112),
        Color(160, 160, 164),
    },

    -- Slate
    {
        Color(206, 220, 227),
        palette[2],
        Color(157, 185, 200),
        Color( 85, 128, 151),
        Color( 85, 128, 151),
        palette[4],
    },

    -- Spruce
    {
        Color(208, 227, 211),
        palette[2],
        Color(162, 200, 169),
        Color( 89, 151, 100),
        Color( 89, 151, 100),
        palette[4],
    },

    -- Wheat
    {
        Color(238, 238, 208),
        palette[2],
        Color(222, 222, 160),
        Color(188, 188,  65),
        palette[8],
    },

    -- Windows Professional
    {
        palette[1],
        Color( 64,  64,  64),
        Color(212, 208, 200),
        palette[4],
        Color( 10,  36, 106),
    },

    -- Amora Focus
    {
        Color( 56,  56,  56),
        Color( 20,  20,  20),
        Color( 26,  26,  26),
        Color( 23,  23,  23),
        Color( 99,  78, 117),
        Color(222, 219, 235),
        Color( 26,  26,  26),
    },
}

local function Box9x(x, y, w, h, scheme)
    surface.SetDrawColor(scheme[3]:Unpack())
    surface.DrawLine(x, y, x + w - 1, y)
    surface.DrawLine(x, y + 1, x, y + h - 1)

    surface.SetDrawColor(scheme[2]:Unpack())
    surface.DrawLine(x, y + h - 1, x + w - 1, y + h - 1)
    surface.DrawLine(x + w - 1, y, x + w - 1, y + h)

    surface.SetDrawColor(scheme[1]:Unpack())
    surface.DrawLine(x + 1, y + 1, x + w - 2, y + 1)
    surface.DrawLine(x + 1, y + 2, x + 1, y + h - 2)

    surface.SetDrawColor(scheme[4]:Unpack())
    surface.DrawLine(x + w - 2, y + 1, x + w - 2, y + h - 2)
    surface.DrawLine(x + 1, y + h - 2, x + w - 1, y + h - 2)

    local bgCol = scheme[7] or scheme[4]
    surface.SetDrawColor(bgCol:Unpack())
    surface.DrawRect(x + 3, y + 3, w - 6, h - 6)

    surface.SetDrawColor(scheme[3]:Unpack())
    surface.DrawOutlinedRect(x + 2, y + 2, w - 4, h - 4)
end

local function Dropdown9x(x, y, w, h, selected, selColor, scheme)
    selected = selected or false

    surface.SetDrawColor(scheme[4]:Unpack())
    surface.DrawLine(x, y, x + w - 1, y)
    surface.DrawLine(x, y + 1, x, y + h - 1)

    surface.SetDrawColor(scheme[1]:Unpack())
    surface.DrawLine(x, y + h - 1, x + w - 1, y + h - 1)
    surface.DrawLine(x + w - 1, y, x + w - 1, y + h)

    surface.SetDrawColor(scheme[2]:Unpack())
    surface.DrawLine(x + 1, y + 1, x + w - 2, y + 1)
    surface.DrawLine(x + 1, y + 2, x + 1, y + h - 2)

    surface.SetDrawColor(scheme[3]:Unpack())
    surface.DrawLine(x + w - 2, y + 1, x + w - 2, y + h - 2)
    surface.DrawLine(x + 1, y + h - 2, x + w - 1, y + h - 2)

    ----

    surface.SetDrawColor(scheme[3]:Unpack())
    surface.DrawLine(x + w - 18, y + 2, x + w - 3, y + 2)
    surface.DrawLine(x + w - 18, y + 3, x + w - 18, y + h - 3)

    surface.SetDrawColor(scheme[2]:Unpack())
    surface.DrawLine(x + w - 18, y + h - 3, x + w - 2, y + h - 3)
    surface.DrawLine(x + w - 3, y + 2, x + w - 3, y + h - 3)

    surface.SetDrawColor(scheme[1]:Unpack())
    surface.DrawLine(x + w - 17, y + 3, x + w - 4, y + 3)
    surface.DrawLine(x + w - 17, y + 4, x + w - 17, y + h - 4)

    surface.SetDrawColor(scheme[4]:Unpack())
    surface.DrawLine(x + w - 17, y + h - 4, x + w - 3, y + h - 4)
    surface.DrawLine(x + w - 4, y + 3, x + w - 4, y + h - 4)

    surface.SetDrawColor(scheme[3]:Unpack())
    surface.DrawRect(x + w - 16, y + 4, 12, h - 8)

    local text = scheme[6] or scheme[2]
    surface.SetDrawColor(text:Unpack())
    surface.DrawLine(x + w - 14, y + 8, x + w - 7, y + 8)
    surface.DrawLine(x + w - 13, y + 9, x + w - 8, y + 9)
    surface.DrawLine(x + w - 12, y + 10, x + w - 9, y + 10)
    surface.DrawLine(x + w - 11, y + 11, x + w - 10, y + 11)

    ----

    local bg = scheme[7] or palette[1]

    surface.SetDrawColor(bg:Unpack())
    surface.DrawRect(x + 2, y + 2, w - 20, h - 4)

    if selected then
        surface.SetDrawColor(selColor:Unpack())
        surface.DrawRect(x + 3, y + 3, w - 22, h - 6)
    end
end

local sv_maxvelocity = GetConVar("sv_maxvelocity")
local function Draw()
    local lply = CHUD:GetLocalPlayer()

    local schemeIndex = CHUD:GetInt("9xColorScheme")
    local scheme = schemes[schemeIndex] or schemes[1]

    local barColorIndex = CHUD:GetInt("9xBarColor")
    local barColor = palette[barColorIndex] or palette[14]

    local col = schemeIndex == 1 and (CHUD:GetBool("9xUseHUDColor") and CHUD:GetHUDColor() or barColor) or scheme[5]
    local textColor = CHUD:GetReadableColor(col)

    local health = math.max(0, lply:Health())
    local armor = math.max(0, lply:Armor())
    local velocity = math.Round(lply:GetVelocity():Length())

    local maxHealth = lply:GetMaxHealth()
    local maxArmor = armor > 100 and math.max(200, lply:GetMaxArmor()) or lply:GetMaxArmor()
    local maxVelocity = sv_maxvelocity:GetInt()

    local healthRatio = math.min(health / maxHealth, 1)
    local armorRatio = math.min(armor / maxArmor, 1)
    local velocityRatio = math.min(velocity / maxVelocity, 1)

    local x, y = 32, ScrH() - 32 - 24
    local w, h = 256, 24

    if (health == maxHealth and not CHUD:GetBool("HideHPAtFull")) or health ~= maxHealth then
        local healthColor = schemeIndex == 1 and (CHUD:GetBool("9xUseHUDColor") and (healthRatio <= 0.25 and CHUD:GetDamageColor() or CHUD:GetHUDColor()) or barColor) or scheme[5]
        local healthTextColor = CHUD:GetReadableColor(healthColor)

        Box9x(x, y, w, h, scheme)
        if (healthRatio <= 0.25 and math.ceil(RealTime() * 3) % 2 == 0) or healthRatio > 0.25 then
            surface.SetDrawColor(healthColor:Unpack())
            surface.DrawRect(x + 3, y + 3, (w - 6) * healthRatio, h - 6)
        end

        local healthString = ("Health: %d/%d"):format(health, maxHealth)
        surface.SetFont("CHUD-Win9x-Bold")
        surface.SetTextColor(healthTextColor:Unpack())
        surface.SetTextPos(x + 6, y + 5)
        surface.DrawText(healthString)

        y = y - 24 - 8
    end

    if (armor == maxArmor and not CHUD:GetBool("HideArmorAtFull")) or armor ~= maxArmor and armor ~= 0 and health ~= 0 then
        Box9x(x, y, w, h, scheme)
        surface.SetDrawColor(col:Unpack())
        surface.DrawRect(x + 3, y + 3, (w - 6) * armorRatio, h - 6)

        local armorString = ("Armor: %d/%d"):format(armor, maxArmor)
        surface.SetFont("CHUD-Win9x-Bold")
        surface.SetTextColor(textColor:Unpack())
        surface.SetTextPos(x + 6, y + 5)
        surface.DrawText(armorString)

        y = y - 24 - 8
    end

    if CHUD:GetBool("ShowVelocity") then
        Box9x(x, y, w, h, scheme)
        surface.SetDrawColor(col:Unpack())
        surface.DrawRect(x + 3, y + 3, (w - 6) * velocityRatio, h - 6)

        local velocityString = ("Velocity: %dups"):format(velocity)
        surface.SetFont("CHUD-Win9x-Bold")
        surface.SetTextColor(textColor:Unpack())
        surface.SetTextPos(x + 6, y + 5)
        surface.DrawText(velocityString)

        local vw = surface.GetTextSize(maxVelocity)
        surface.SetTextColor(textColor:Unpack())
        surface.SetTextPos(x + w - 6 - vw, y + 5)
        surface.DrawText(maxVelocity)
    end

    local wep = lply:GetActiveWeapon()

    if IsValid(wep) then
        x = CHUD:GetBool("9xAmmoLeft") and x + 256 + 16 or ScrW() - w - 32
        y = ScrH() - 32 - 24

        if wep:GetPrimaryAmmoType() ~= -1 then
            local ammo = wep:Clip1()
            local maxAmmo = wep:GetMaxClip1()
            local reserve = lply:GetAmmoCount(wep:GetPrimaryAmmoType())
            local maxType = game.GetAmmoMax(wep:GetPrimaryAmmoType())
            local ammoRatio = (ammo == -1 or maxAmmo == -1) and math.min(reserve / maxType, 1) or math.min(ammo / maxAmmo, 1)

            Box9x(x, y, w, h, scheme)
            surface.SetDrawColor(col:Unpack())
            surface.DrawRect(x + 3, y + 3, (w - 6) * ammoRatio, h - 6)

            local ammoString = (ammo == -1 or maxAmmo == -1) and ("Ammo: %d"):format(reserve) or ("Ammo: %d/%d"):format(ammo, maxAmmo)
            surface.SetFont("CHUD-Win9x-Bold")
            surface.SetTextColor(textColor:Unpack())
            surface.SetTextPos(x + 6, y + 5)
            surface.DrawText(ammoString)

            if ammo ~= -1 and maxAmmo ~= -1 then
                local rw = surface.GetTextSize(reserve)

                surface.SetFont("CHUD-Win9x-Bold")
                surface.SetTextColor(textColor:Unpack())
                surface.SetTextPos(x + w - rw - 6, y + 5)
                surface.DrawText(reserve)
            end

            y = y - 24 - 8
        end

        if wep:GetSecondaryAmmoType() ~= -1 then
            local ammo = lply:GetAmmoCount(wep:GetSecondaryAmmoType())
            local maxAmmo = game.GetAmmoMax(wep:GetSecondaryAmmoType())
            local ammoRatio = math.min(ammo / maxAmmo, 1)

            Box9x(x, y, w, h, scheme)
            surface.SetDrawColor(col:Unpack())
            surface.DrawRect(x + 3, y + 3, (w - 6) * ammoRatio, h - 6)

            local ammoString = ("Alt: %d"):format(ammo)
            surface.SetFont("CHUD-Win9x-Bold")
            surface.SetTextColor(textColor:Unpack())
            surface.SetTextPos(x + 6, y + 5)
            surface.DrawText(ammoString)

            y = y - 24 - 8
        end
    end
end

local function DrawWeaponSwitcher(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength)
    if RealTime() > flSelectTime + 2.5 then
        iCurSlot = 0
        iCurPos = 1

        return
    end

    local schemeIndex = CHUD:GetInt("9xColorScheme")
    local scheme = schemes[schemeIndex] or schemes[1]

    local barColorIndex = CHUD:GetInt("9xBarColor")
    local barColor = palette[barColorIndex] or palette[14]

    local col = schemeIndex == 1 and (CHUD:GetBool("9xUseHUDColor") and CHUD:GetHUDColor() or barColor) or scheme[5]
    local textColor = CHUD:GetReadableColor(col)

    local width = MAX_SLOTS * 192
    local x, y = ScrW() / 2 - width / 2, 96
    Box9x(x - 3, y - 3, width + 6, 46, scheme)

    surface.SetDrawColor(col:Unpack())
    surface.DrawRect(x, y, width, 18)

    surface.SetFont("CHUD-Win9x-Bold")
    surface.SetTextColor(textColor:Unpack())
    surface.SetTextPos(x + 3, y + 2)
    surface.DrawText("Weapon Switcher")

    surface.SetDrawColor(scheme[3]:Unpack())
    surface.DrawLine(x, y + 18, x + width, y + 18)

    y = y + 19

    for i = 1, MAX_SLOTS do
        Dropdown9x(x, y, 192, 21, iCurSlot == i, col, scheme)
        surface.SetFont("CHUD-Win9x")
        surface.SetTextColor((iCurSlot == i and textColor or scheme[6] or palette[2]):Unpack())
        surface.SetTextPos(x + 5, y + 4)
        surface.DrawText("Slot " .. i)

        if iCurSlot == i then
            surface.SetDrawColor(scheme[2]:Unpack())
            surface.DrawOutlinedRect(x, y + 21, 192, tCacheLength[iCurSlot] * 15 + 2)

            local bg = scheme[7] or palette[1]
            surface.SetDrawColor(bg:Unpack())
            surface.DrawRect(x + 1, y + 22, 190, tCacheLength[iCurSlot] * 15)

            for index, wep in ipairs(tCache[iCurSlot]) do
                if not IsValid(wep) then continue end
                if iCurPos == index then
                    surface.SetDrawColor(col:Unpack())
                    surface.DrawRect(x + 1, y + 22 + (15 * (index - 1)), 190, 15)
                end
                surface.SetFont("CHUD-Win9x")
                surface.SetTextColor((iCurPos == index and textColor or scheme[6] or palette[2]):Unpack())
                surface.SetTextPos(x + 6, y + 22 + (15 * (index - 1)) + 1)
                surface.DrawText(CHUD.WeaponNames[wep:GetClass()] or (wep.PrintName or language.GetPhrase(wep:GetClass())))
            end
        end

        x = x + 192
    end
end

CHUD.Styles["9x"] = {
    Name = "9x",
    Draw = Draw,
    Demo = function() end,
    HasWeaponSwitcher = true,
    WeaponSwitcher = DrawWeaponSwitcher,
}

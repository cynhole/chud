CHUD = CHUD or {}

CHUD.CurResW = ScrW()
CHUD.CurResH = ScrH()

CHUD.CVars = {
    Enabled             = CreateClientConVar("chud_enabled",               "1",           true, false, "Enable/Disable CHUD"),
    WeaponSelector      = CreateClientConVar("chud_wpn_enabled",           "1",           true, false, "Enable/Disable Weapon Switcher"),
    Style               = CreateClientConVar("chud_style",                 "hl2",         true, false, "CHUD Style"),
    StyleWeapon         = CreateClientConVar("chud_style_wpn",             "hl2",         true, false, "Style for Weapon Switcher"),
    Debug               = CreateClientConVar("chud_debug",                 "0",           true, false, "Unhide the default HUD to compare"),

    QuickInfo           = CreateClientConVar("chud_quickinfo",             "1",           true, false, "Enable/Disable Quick Info"),
    QuickInfoSounds     = CreateClientConVar("chud_quickinfo_snd",         "1",           true, false, "Enable Quick Info warning sounds"),
    QuickInfoTwoBars    = CreateClientConVar("chud_quickinfo_twobars",     "0",           true, false, "Enable bars for armor and ammo reserve"),

    Compass             = CreateClientConVar("chud_compass",               "0",           true, false, "Enable/Disable Compass"),
    CompassMultiplier   = CreateClientConVar("chud_compass_multiplier",    "1",           true, false, "Width multiplier"),
    CompassUseDmg       = CreateClientConVar("chud_compass_dmg_col",       "1",           true, false, "Use damage color for north"),
    CompassTop          = CreateClientConVar("chud_compass_top",           "1",           true, false, "Put compass at the top of the screen"),
    CompassLabels       = CreateClientConVar("chud_compass_labels",        "1",           true, false, "Add labels to cardnal directions"),
    CompassLabelsBottom = CreateClientConVar("chud_compass_labels_bottom", "0",           true, false, "Put labels below the tickmarks"),
    CompassTicks        = CreateClientConVar("chud_compass_ticks",         "16",          true, false, "How many tickmarks should exist"),
    CompassSubCardnals  = CreateClientConVar("chud_compass_subcardnals",   "0",           true, false, "Display sub cardnal directions (NE, NW, SE, SW)"),

    CustomColor         = CreateClientConVar("chud_customcolor",           "255 220 0",   true, false, "Custom HUD color"),
    DamageColor         = CreateClientConVar("chud_damagecolor",           "255 0 0",     true, false, "Custom damage color for health when under 25 HP"),
    BackgroundColor     = CreateClientConVar("chud_backgroundcolor",       "0 0 0",       true, false, "Custom background color"),
    BackgroundAlpha     = CreateClientConVar("chud_backgroundalpha",       "80",          true, false, "Alpha for background on HUD elements"),
    UsePlayerColor      = CreateClientConVar("chud_useplayercolor",        "0",           true, false, "Use player color for HUD color"),
    UseWeaponColor      = CreateClientConVar("chud_useweaponcolor",        "0",           true, false, "Use weapon color for HUD color"),
    UseTeamColor        = CreateClientConVar("chud_useteamcolor",          "0",           true, false, "Use team color for HUD color"),
    UsePlayerColorBG    = CreateClientConVar("chud_useplayercolor_bg",     "0",           true, false, "Use player color for background color"),
    UseWeaponColorBG    = CreateClientConVar("chud_useweaponcolor_bg",     "0",           true, false, "Use weapon color for background color"),
    UseTeamColorBG      = CreateClientConVar("chud_useteamcolor_bg",       "0",           true, false, "Use team color for background color"),
    UsePlayerColorDMG   = CreateClientConVar("chud_useplayercolor_dmg",    "0",           true, false, "Use player color for damaged color"),
    UseWeaponColorDMG   = CreateClientConVar("chud_useweaponcolor_dmg",    "0",           true, false, "Use weapon color for damaged color"),
    UseTeamColorDMG     = CreateClientConVar("chud_useteamcolor_dmg",      "0",           true, false, "Use team color for damaged color"),

    Rainbow             = CreateClientConVar("chud_rainbow",               "0",           true, false, "Use Rainbow/Chroma for HUD color"),
    RainbowSpeed        = CreateClientConVar("chud_rbow_speed",            "2",           true, false, "Rainbow change speed"),
    RainbowSat          = CreateClientConVar("chud_rbow_sat",              "1",           true, false, "Rainbow saturation, decimal value 0-1"),
    RainbowValue        = CreateClientConVar("chud_rbow_value",            "1",           true, false, "Rainbow value/brightness, decimal value 0-1"),
    RainbowBG           = CreateClientConVar("chud_rainbow_bg",            "0",           true, false, "Use Rainbow/Chroma for background color"),
    RainbowSpeedBG      = CreateClientConVar("chud_rbow_speed_bg",         "2",           true, false, "[Backgrounnd] Rainbow change speed"),
    RainbowSatBG        = CreateClientConVar("chud_rbow_sat_bg",           "1",           true, false, "[Backgrounnd] Rainbow saturation, decimal value 0-1"),
    RainbowValueBG      = CreateClientConVar("chud_rbow_value_bg",         "1",           true, false, "[Backgrounnd] Rainbow value/brightness, decimal value 0-1"),

    ShowVelocity        = CreateClientConVar("chud_showvelocity",          "0",           true, false, "Show velocity on HUD"),
    LeftVelocity        = CreateClientConVar("chud_vel_left",              "0",           true, false, "Left align velocity value"),
    VelocityAltPos      = CreateClientConVar("chud_vel_altpos",            "0",           true, false, "Alternate velocity position, 0 - above health, 1 - next to health/armor"),
    DisableExtras       = CreateClientConVar("chud_disable_extras",        "1",           true, false, "Disable extra, unused/annoying HUD elements"),
    HideHPAtFull        = CreateClientConVar("chud_hidehpatfull",          "0",           true, false, "Hide health at 100 HP"),
    HideArmorAtFull     = CreateClientConVar("chud_hidearmoratfull",       "0",           true, false, "Hide armor at 100/200"),
    HideAltWhenEmpty    = CreateClientConVar("chud_hidealtwhenempty",      "0",           true, false, "Hide alt ammo when empty"),
    ClampNegative       = CreateClientConVar("chud_negativehp",            "0",           true, false, "Should negative HP show, 0 - clamp at 0, 1 - show negative"),
    AltAmmoOldPos       = CreateClientConVar("chud_altammo_oldpos",        "1",           true, false, "Alternate Ammo at old position, 0 - above ammo, 1 - regular"),
    AmmoIcons           = CreateClientConVar("chud_ammo_icons",            "1",           true, false, "Show ammo icons"),
    AmmoIconsSecondary  = CreateClientConVar("chud_ammo_icons_alt",        "0",           true, false, "Show ammo icons on secondary ammo"),
    HUDIcons            = CreateClientConVar("chud_hud_icons",             "0",           true, false, "Show other icons above labels"),
    AdditiveFonts       = CreateClientConVar("chud_additivefonts",         "1",           true, false, "Should fonts be additive/transparent. 0 - no additive, more visible in brightness"),

    CrosshairStyle      = CreateClientConVar("chud_crosshair",             "0",           true, false, "Crosshair Style, -1 - disabled, 0 - default, 1 - dot, 2 - vehicle, 3 - vehicle 2, 4 - dynamic"),
    CrosshairLength     = CreateClientConVar("chud_crosshair_length",      "8",           true, false, "Crosshair length, only works on crosshair 4"),
    CrosshairSize       = CreateClientConVar("chud_crosshair_size",        "2",           true, false, "Crosshair size, only works on crosshair 4"),
    CrosshairSpace      = CreateClientConVar("chud_crosshair_space",       "4",           true, false, "Crosshair space, only works on crosshair 4"),
    CrosshairOutline    = CreateClientConVar("chud_crosshair_outline",     "1",           true, false, "Crosshair outline, only works on crosshair 4"),
    CrosshairDynamic    = CreateClientConVar("chud_crosshair_dynamic",     "1",           true, false, "Crosshair dynamic mode, only works on crosshair 4"),
    CrosshairColor      = CreateClientConVar("chud_crosshair_color",       "255 255 255", true, false, "Crosshair color"),
    CrosshairAlpha      = CreateClientConVar("chud_crosshair_alpha",       "255",         true, false, "Crosshair alpha"),
    CrosshairUseHUD     = CreateClientConVar("chud_crosshair_usehud",      "1",           true, false, "Crosshair color uses HUD color"),
    CrosshairOverride   = CreateClientConVar("chud_crosshair_override",    "0",           true, false, "Crosshair overrides weapon coded crosshair"),
    CrosshairTFA        = CreateClientConVar("chud_crosshair_tfa",         "1",           true, false, "Crosshair overrides TFA's crosshair"),
    CrosshairCW2        = CreateClientConVar("chud_crosshair_cw",          "1",           true, false, "Crosshair overrides CW2.0's crosshair"),
}

concommand.Add("chud_reload",function()
    include("chud/chud.lua")
end)

include("chud/utils.lua")

function CHUD.Utils.CreateFonts()
    surface.CreateFont("CHUD-WeaponSelect-HL2",{
        font = "HalfLife2",
        size = ScreenScaleH(64),
        weight = 0,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-WeaponSelected-HL2",{
        font = "HalfLife2",
        size = ScreenScaleH(64),
        weight = 0,
        blursize = ScreenScaleH(4),
        scanlines = ScreenScaleH(2),
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-WeaponSelect-CS",{
        font = "cs",
        size = ScreenScaleH(64),
        weight = 0,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-WeaponSelected-CS",{
        font = "cs",
        size = ScreenScaleH(64),
        weight = 0,
        blursize = ScreenScaleH(4),
        scanlines = ScreenScaleH(2),
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-WeaponSelect-CSD",{
        font = "csd",
        size = ScreenScaleH(64),
        weight = 0,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-WeaponSelected-CSD",{
        font = "csd",
        size = ScreenScaleH(64),
        weight = 0,
        blursize = ScreenScaleH(4),
        scanlines = ScreenScaleH(2),
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-HL2-Crosshairs", {
        font = "HalfLife2",
        antialias = false,
        size = 40,
        weight = 0,
        additive = true,
    })

    surface.CreateFont("CHUD-HL2-QuickInfo", {
        font = "HL2cross",
        size = 63,
        weight = 0,
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-HL2-HUDNumbers",{
        font = "HalfLife2",
        size = ScreenScaleH(32),
        weight = 0,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = CHUD.CVars.AdditiveFonts:GetBool(),
    })

    surface.CreateFont("CHUD-HL2-HUDNumbersGlow",{
        font = "HalfLife2",
        size = ScreenScaleH(32),
        weight = 0,
        blursize = ScreenScaleH(4),
        scanlines = ScreenScaleH(2),
        antialias = true,
        additive = CHUD.CVars.AdditiveFonts:GetBool(),
    })

    surface.CreateFont("CHUD-HL2-HUDNumbersSmall",{
        font = "HalfLife2",
        size = ScreenScaleH(16),
        weight = 1000,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = CHUD.CVars.AdditiveFonts:GetBool(),
    })

    surface.CreateFont("CHUD-HL2-HUDDefault",{
        font = "Verdana",
        size = ScreenScaleH(9),
        weight = 900,
        antialias = true,
    })

    surface.CreateFont("CHUD-HL2-WeaponIconsSmall",{
        font = "HalfLife2",
        size = ScreenScaleH(32),
        weight = 0,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = true,
    })
    surface.CreateFont("CHUD-HL2-WeaponIconsSmall2",{
        font = "HalfLife2",
        size = ScreenScaleH(40),
        weight = 0,
        blursize = 0,
        scanlines = 0,
        antialias = true,
        additive = true,
    })

    surface.CreateFont("CHUD-HUDSelectionNumbers",{
        font = "Verdana",
        size = ScreenScaleH(11),
        weight = 700,
        antialias = true,
        additive = CHUD.CVars.AdditiveFonts:GetBool(),
    })

    surface.CreateFont("CHUD-HUDSelectionText",{
        font = "Verdana",
        size = ScreenScaleH(7.5),
        weight = 700,
        antialias = true,
    })

    surface.CreateFont("CHUD-CS-Icons",{
        font = "Counter-Strike",
        size = ScreenScaleH(28),
        weight = 0,
        antialias = true,
        additive = CHUD.CVars.AdditiveFonts:GetBool(),
    })

    surface.CreateFont("CHUD-Win9x", {
        font = "Verdana",
        size = 13.5,
        weight = 0,
        antialias = false,
    })

    surface.CreateFont("CHUD-Win9x-Bold", {
        font = "Verdana",
        size = 13.5,
        weight = 700,
        antialias = false,
    })
end

CHUD.Utils.CreateFonts()

cvars.AddChangeCallback("chud_additivefonts", CHUD.Utils.CreateFonts, "chud_additive")
cvars.AddChangeCallback("chud_crosshair_tfa", function(_, _, new)
    if tonumber(new) == 1 then
        GetConVar("cl_tfa_hud_crosshair_outline_enabled"):SetBool(false)
        GetConVar("cl_tfa_hud_crosshair_color_a"):SetInt(0)
    else
        GetConVar("cl_tfa_hud_crosshair_outline_enabled"):SetBool(true)
        GetConVar("cl_tfa_hud_crosshair_color_a"):SetInt(255)
    end
end, "chud_crosshair_tfa")
cvars.AddChangeCallback("chud_crosshair_cw", function(_, _, new)
    if tonumber(new) == 1 then
        GetConVar("cw_crosshair"):SetBool(false)
    else
        GetConVar("cw_crosshair"):SetBool(true)
    end
end, "chud_crosshair_cw")

hook.Add("Think", "CHUD.Resolution", function()
    local scrW = ScrW()
    local scrH = ScrH()

    CHUD.CurResW = CHUD.CurResW or scrW
    CHUD.CurResH = CHUD.CurResH or scrH

    local resChanged = false

    if CHUD.CurResW ~= scrW or CHUD.CurResH ~= scrH then
        resChanged = true
    end

    if resChanged then
        CHUD.CurResH = scrH
        CHUD.CurResW = scrW

        CHUD.Utils.CreateFonts()
    end
end)

function CHUD.Destroy()
    hook.Remove("HUDPaint","CHUD")
    hook.Remove("HUDPaint","CHUD.QuickInfo")
    hook.Remove("HUDPaint","CHUD.Compass")
    hook.Remove("HUDPaint", "CHUD.Crosshair")
    hook.Remove("HUDShouldDraw","CHUD")
    hook.Remove("HUDShouldDraw","CHUD.QuickInfo")
    hook.Remove("CreateMove","CHUD.CalcMovement")
    hook.Remove("DrawOverlay","CHUD.WeaponSelector")
    hook.Remove("DrawPhysgunBeam","CHUD.TotallyDidntStealThisFromGoldsrcHUD")
    hook.Remove("Think","CHUD.TotallyDidntStealThisFromGoldsrcHUD")
    hook.Remove("Think","CHUD.Resolution")
    hook.Remove("PlayerBindPress","CHUD_WeaponSelector")
end

CHUD.Styles = CHUD.Styles or {}

include("chud/weapon_switcher/legacy.lua")
include("chud/styles/hl2.lua")
include("chud/styles/css.lua")
include("chud/styles/oc.lua")
include("chud/styles/9x.lua")

include("chud/quickinfo.lua")
include("chud/compass.lua")
include("chud/basehud.lua")
include("chud/weapon_switcher/logic.lua")
include("chud/settings.lua")
include("chud/tfa_colors.lua")
-- https://github.com/Kefta/Weapon-Switcher-Skeleton

--[[ Config ]]--
local MAX_SLOTS = 6	 -- Max number of weapon slots. Expects Integer [0, inf)
local CACHE_TIME = 1 -- Time in seconds between updating the weapon cache. RealTime is used for comparisons. Expects Decimal [0, inf]. 0 = update every frame, inf = never update
local MOVE_SOUND = "Player.WeaponSelectionMoveSlot" -- Sound to play when the player moves between weapon slots. Expects String soundscape or sound file path. "" = no sound
local SELECT_SOUND = "Player.WeaponSelectionClose" -- Sound to play when the player selects a weapon. Expects String soundscape or sound file path. "" = no sound
local CANCEL_SOUND = "" -- Sound to play when the player cancels the weapon selection. Expects String soundscape or sound file path. "" = no sound

--[[ Instance variables - do not edit ]]--

local iCurSlot = 0 -- Currently selected slot. Will be an Integer [0, MAX_SLOTS]. 0 = no selection
local iCurPos = 1 -- Current position in that slot. Will be an Integer [0, inf)
local flNextPrecache = 0 -- Time until next precache. Will be a Decimal [0, inf) representing a RealTime
local flSelectTime = 0 -- Time the weapon selection changed slot/visibility states. Can be used to close the weapon selector after a certain amount of idle time. Will be a Decimal [0, inf) representing a RealTime
local iWeaponCount = 0 -- Total number of weapons on the player. Will be an Integer [0, inf)

-- Weapon cache; table of tables. tCache[Weapon:GetSlot() + 1] or tCacheLength[iCurSlot] contains a table containing that slot's weapons. The table's length is tCacheLength[Slot + 1]
local tCache = {}

-- Weapon cache length. tCacheLength[Weapon:GetSlot() + 1] or tCacheLength[iCurSlot] will contain the number of weapons that slot has
local tCacheLength = {}

--[[ Weapon switcher ]]--

--[[ Guarentees when this function is called:
    - cl_drawhud != 0
    - iCurSlot >= 1
    - 1 <= iCurPos <= tCacheLength[iCurSlot]
    - iWeaponCount >= 1
    - LocalPlayer():IsValid()
    - LocalPlayer():Alive()
    - not LocalPlayer():InVehicle() or LocalPlayer():GetAllowWeaponsInVehicle()
]]

--[[ Implementation - do not edit ]]--

-- Initialize tables with slot number
for i = 1, MAX_SLOTS do
    tCache[i] = {}
    tCacheLength[i] = 0
end

local tonumber = tonumber
local RealTime = RealTime
local hook_Add = hook.Add
local math_floor = math.floor
local string_sub = string.sub
local string_lower = string.lower
local input_SelectWeapon = input.SelectWeapon

local function PrecacheWeps()
    -- Reset all table values
    for i = 1, MAX_SLOTS do
        for j = 1, tCacheLength[i] do
            tCache[i][j] = nil
        end

        tCacheLength[i] = 0
    end

    -- Update the cache time
    flNextPrecache = RealTime() + CACHE_TIME

    local tWeapons = CHUD:GetLocalPlayer():GetWeapons()
    iWeaponCount = #tWeapons

    if (iWeaponCount == 0) then
        iCurSlot = 0
        iCurPos = 1
    else
        for i = 1, iWeaponCount do
            local pWeapon = tWeapons[i]

            -- Weapon slots start internally at 0
            -- Here, we will start at 1 to match the slot binds
            local iSlot = pWeapon:GetSlot() + 1

            if (iSlot <= MAX_SLOTS) then
                -- Cache number of weapons in each slot
                local iLen = tCacheLength[iSlot] + 1
                tCacheLength[iSlot] = iLen
                tCache[iSlot][iLen] = pWeapon
            end
        end

        for i = 1, MAX_SLOTS do
            table.sort(tCache[i], function(a, b) return b:GetSlotPos() > a:GetSlotPos() end)
        end
    end

    -- Make sure we're not pointing out of bounds
    if (iCurSlot ~= 0) then
        local iLen = tCacheLength[iCurSlot]

        if (iLen == 0) then
            iCurSlot = 0
            iCurPos = 1
        elseif (iCurPos > iLen) then
            iCurPos = iLen
        end
    end
end

local function CheckBounds()
    if (iCurSlot < 0 or iCurSlot > MAX_SLOTS) then
        iCurSlot = 0
    else
        iCurSlot = math_floor(iCurSlot)
    end

    if (iCurPos < 1) then
        iCurPos = 1
    else
        iCurPos = math_floor(iCurPos)
    end

    if (iWeaponCount < 0) then
        iWeaponCount = 0
    else
        iWeaponCount = math_floor(iWeaponCount)
    end
end

local hud_fastswitch = GetConVar("hud_fastswitch")
local function WeaponCheck(ply)
    if hud_fastswitch:GetBool() then
        return true
    end

    local wep = ply:GetActiveWeapon()
    if IsValid(wep) and (wep:GetClass() == "weapon_physgun" or wep:GetClass() == "weapon_newtphysgun") then
        return ply:KeyDown(IN_ATTACK)
    end

    local traceEnt = ply:GetEyeTrace().Entity
    local entInputs = IsValid(traceEnt) and WireLib and WireLib.GetPorts(traceEnt)

    if IsValid(wep) and wep:GetClass() == "gmod_tool" and wep.current_mode == "wire_adv" and (entInputs or wep.Tool.wire_adv:GetStage() == 2) then
        return true
    end

    if IsValid(wep) and wep:GetClass() == "gmod_tool" and wep.current_mode == "submaterial" and IsValid(traceEnt) then
        return true
    end

    if ArcCW and IsValid(wep) and wep.ArcCW and wep:GetState() == ArcCW.STATE_SIGHTS then
        return true
    end
end

local w_alpha = 1
local wpnFading = false

hook_Add("DrawOverlay", CHUD:HTag("WeaponSwitcher"), function()
    if not CHUD:GetBool("Enabled") or not CHUD:GetBool("WeaponSelector") then return end

    local style = CHUD.Styles[CHUD:GetString("StyleWeapon")]

    if not style or not style.WeaponSwitcher or not isfunction(style.WeaponSwitcher) then
        style = CHUD.Styles["hl2"]
    end

    if not CHUD.Styles["hl2"] then
        include("chud/chud.lua")
        return
    end

    if iCurSlot == 0 then return end

    local show
    show = show or RealTime() < flSelectTime + 2
    w_alpha = Lerp(RealFrameTime() * 3.5, w_alpha, show and 1 or 0)
    if w_alpha <= 0.005 then
        iCurSlot = 0

        flSelectTime = RealTime()
        w_alpha = 1
        wpnFading = false
        return
    end

    if w_alpha < 1 then
        wpnFading = true
    end

    CheckBounds()

    if (iCurSlot == 0) then
        return
    end

    local pPlayer = CHUD:GetLocalPlayer()

    -- Don't draw in vehicles unless weapons are allowed to be used
    -- Or while dead!
    if (pPlayer:IsValid() and pPlayer:Alive() and (not pPlayer:InVehicle() or pPlayer:GetAllowWeaponsInVehicle())) then
        if (flNextPrecache <= RealTime()) then
            PrecacheWeps()
        end

        if (iCurSlot ~= 0) then
            surface.SetAlphaMultiplier(w_alpha)
            style.WeaponSwitcher(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength)
            surface.SetAlphaMultiplier(1)
        end
    else
        iCurSlot = 0
        iCurPos = 1
    end
end)

hook_Add("PlayerBindPress", CHUD:HTag("WeaponSwitcher"), function(pPlayer, sBind, bPressed)
    if not CHUD:GetBool("Enabled") or not CHUD:GetBool("WeaponSelector") then return end

    if (not pPlayer:Alive() or pPlayer:InVehicle() and not pPlayer:GetAllowWeaponsInVehicle()) then
        return
    end

    local wep = pPlayer:GetActiveWeapon()

    if (IsValid(wep) and wep:GetClass() ~= "gmod_camera") and hook.Run("HUDShouldDraw", "_chud_everything") == false then return end
    if (IsValid(wep) and wep:GetClass() ~= "gmod_camera") and hook.Run("HUDShouldDraw", "_chud_weaponswitcher") == false then return end

    if input.IsKeyDown(KEY_LALT) then return end

    sBind = string_lower(sBind)

    -- Close the menu
    if (sBind == "cancelselect") then
        if (bPressed and iCurSlot ~= 0) then
            iCurSlot = 0
            iCurPos = 1

            flSelectTime = RealTime()
            w_alpha = 1
            pPlayer:EmitSound(CANCEL_SOUND)
        end

        return true
    end

    -- Move to the weapon before the current
    if (sBind == "invprev") and not WeaponCheck(pPlayer) then
        if (not bPressed) then
            return true
        end

        if wpnFading == true then wpnFading = false end

        CheckBounds()
        PrecacheWeps()

        if (iWeaponCount == 0) then
            return true
        end

        local bLoop = iCurSlot == 0

        if (bLoop) then
            local pActiveWeapon = pPlayer:GetActiveWeapon()

            if (pActiveWeapon:IsValid()) then
                local iSlot = pActiveWeapon:GetSlot() + 1
                local tSlotCache = tCache[iSlot]

                if (tSlotCache[1] ~= pActiveWeapon) then
                    iCurSlot = iSlot
                    iCurPos = 1

                    for i = 2, tCacheLength[iSlot] do
                        if (tSlotCache[i] == pActiveWeapon) then
                            iCurPos = i - 1

                            break
                        end
                    end

                    flSelectTime = RealTime()
                    w_alpha = 1
                    pPlayer:EmitSound(MOVE_SOUND)

                    return true
                end

                iCurSlot = iSlot
            end
        end

        if (bLoop or iCurPos == 1) then
            repeat
                if (iCurSlot <= 1) then
                    iCurSlot = MAX_SLOTS
                else
                    iCurSlot = iCurSlot - 1
                end
            until (tCacheLength[iCurSlot] ~= 0)

            iCurPos = tCacheLength[iCurSlot]
        else
            iCurPos = iCurPos - 1
        end

        flSelectTime = RealTime()
        w_alpha = 1
        pPlayer:EmitSound(MOVE_SOUND)

        return true
    end

    -- Move to the weapon after the current
    if (sBind == "invnext") and not WeaponCheck(pPlayer) then
        if (not bPressed) then
            return true
        end

        if wpnFading == true then wpnFading = false end

        CheckBounds()
        PrecacheWeps()

        -- Block the action if there aren't any weapons available
        if (iWeaponCount == 0) then
            return true
        end

        -- Lua's goto can't jump between child scopes
        local bLoop = iCurSlot == 0

        -- Weapon selection isn't currently open, move based on the active weapon's position
        if (bLoop) then
            local pActiveWeapon = pPlayer:GetActiveWeapon()

            if (pActiveWeapon:IsValid()) then
                local iSlot = pActiveWeapon:GetSlot() + 1
                local iLen = tCacheLength[iSlot]
                local tSlotCache = tCache[iSlot]

                if (tSlotCache[iLen] ~= pActiveWeapon) then
                    iCurSlot = iSlot
                    iCurPos = 1

                    for i = 1, iLen - 1 do
                        if (tSlotCache[i] == pActiveWeapon) then
                            iCurPos = i + 1

                            break
                        end
                    end

                    flSelectTime = RealTime()
                    w_alpha = 1
                    pPlayer:EmitSound(MOVE_SOUND)

                    return true
                end

                -- At the end of a slot, move to the next one
                iCurSlot = iSlot
            end
        end

        if (bLoop or iCurPos == tCacheLength[iCurSlot]) then
            -- Loop through the slots until one has weapons
            repeat
                if (iCurSlot == MAX_SLOTS) then
                    iCurSlot = 1
                else
                    iCurSlot = iCurSlot + 1
                end
            until (tCacheLength[iCurSlot] ~= 0)

            -- Start at the beginning of the new slot
            iCurPos = 1
        else
            -- Bump up the position
            iCurPos = iCurPos + 1
        end

        flSelectTime = RealTime()
        w_alpha = 1
        pPlayer:EmitSound(MOVE_SOUND)

        return true
    end

    -- Keys 1-6
    if (string_sub(sBind, 1, 4) == "slot") then
        local iSlot = tonumber(string_sub(sBind, 5))

        if IsValid(LUAHUDMENU_PANEL) and LUAHUDMENU_PANEL:IsMenuOpen() then return end

        -- If the command is slot#, use it for the weapon HUD
        -- Otherwise, let it pass through to prevent false positives
        if (iSlot == nil) then
            return
        end

        if (not bPressed) then
            return true
        end

        if wpnFading == true then wpnFading = false end

        CheckBounds()
        PrecacheWeps()

        -- Play a sound even if there aren't any weapons in that slot for "haptic" (really auditory) feedback
        if (iWeaponCount == 0) then
            pPlayer:EmitSound(MOVE_SOUND)

            return true
        end

        -- If the slot number is in the bounds
        if (iSlot <= MAX_SLOTS) then
            -- If the slot is already open
            if (iSlot == iCurSlot) then
                -- Start back at the beginning
                if (iCurPos == tCacheLength[iCurSlot]) then
                    iCurPos = 1
                -- Move one up
                else
                    iCurPos = iCurPos + 1
                end
            -- If there are weapons in this slot, display them
            elseif (tCacheLength[iSlot] ~= 0) then
                iCurSlot = iSlot
                iCurPos = 1
            end

            flSelectTime = RealTime()
            w_alpha = 1
            pPlayer:EmitSound(MOVE_SOUND)
        end

        return true
    end

    -- If the weapon selection is currently open
    if (iCurSlot ~= 0) then
        if (sBind == "+attack") then
            if wpnFading == true then wpnFading = false end

            -- Hide the selection
            local pWeapon = tCache[iCurSlot][iCurPos]
            iCurSlot = 0
            iCurPos = 1

            -- If the weapon still exists and isn't the player's active weapon
            if (pWeapon:IsValid() and pWeapon ~= pPlayer:GetActiveWeapon()) then
                input_SelectWeapon(pWeapon)
            end

            flSelectTime = RealTime()
            w_alpha = 1
            pPlayer:EmitSound(SELECT_SOUND)

            return true
        end

        -- Another shortcut for closing the selection
        if (sBind == "+attack2") then
            flSelectTime = RealTime()
            w_alpha = 1
            pPlayer:EmitSound(CANCEL_SOUND)

            iCurSlot = 0
            iCurPos = 1

            return true
        end
    end
end)
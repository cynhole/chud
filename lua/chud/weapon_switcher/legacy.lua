local draw = draw
local language = language
local surface = surface

local Material = Material
local IsValid = IsValid

local draw_DrawText = draw.DrawText
local draw_RoundedBox = draw.RoundedBox
local language_GetPhrase = language.GetPhrase
local surface_SetDrawColor = surface.SetDrawColor
local surface_SetMaterial = surface.SetMaterial
local surface_SetTexture = surface.SetTexture
local surface_DrawTexturedRect = surface.DrawTexturedRect
local surface_GetTextureID = surface.GetTextureID

local colText = CHUD:GetHUDColor()
local colBack = CHUD:GetBackgroundColor()

local WeaponIconsHL2 = {
    weapon_smg1        = "a",
    weapon_shotgun     = "b",
    weapon_crowbar     = "c",
    weapon_pistol      = "d",
    weapon_357         = "e",
    weapon_crossbow    = "g",
    weapon_physgun     = "h",
    weapon_rpg         = "i",
    weapon_bugbait     = "j",
    weapon_frag        = "k",
    weapon_ar2         = "l",
    weapon_physcannon  = "m",
    weapon_stunstick   = "n",
    weapon_slam        = "o",
    weapon_medkit      = "+",
    hands              = "C",
    none               = "C",
    weapon_slap        = "\xe2\x9a\xa1",
    weapon_annabelle   = "(",
    weapon_alyxgun     = "%",

    -- source-weps
    weapon_smg1r      = "a",
    weapon_shotgunr   = "b",
    weapon_crowbarr   = "c",
    weapon_pistolr    = "d",
    weapon_357r       = "e",
    weapon_357s       = "e",
    weapon_crossbowr  = "g",
    weapon_rpgr       = "i",
    weapon_bugbaitr   = "j",
    weapon_fragr      = "k",
    weapon_ar2r       = "l",
    weapon_stunstickr = "n",
    weapon_slamr      = "o",
    weapon_alyxgunr   = "%",
    weapon_annabeller = "(",

    -- unobtanable stuff
    weapon_cubemap         = "",
    weapon_citizenpackage  = "",
    weapon_citizensuitcase = "",
    weapon_oldmanharpoon   = "",

    -- hl1
    weapon_357_hl1      = "",
    weapon_crossbow_hl1 = "",
    weapon_crowbar_hl1  = "",
    weapon_egon         = "",
    weapon_gauss        = "h",
    weapon_glock_hl1    = "",
    weapon_handgrenade  = "",
    weapon_hornetgun    = "",
    weapon_mp5_hl1      = "",
    weapon_rpg_hl1      = "",
    weapon_satchel      = "",
    weapon_shotgun_hl1  = "",
    weapon_snark        = "",
    weapon_tripmine     = "",

    -- misc
    weapon_cyn_rr         = "$",

    weapon_archerxbow     = ")",
    weapon_coilgun        = ":",
    weapon_plasmanade     = "_",
    weapon_scrapper       = "z",
    weapon_asmd           = "f",

    oc_grapple            = "!",
    weapon_instagib       = "f",

    weapon_throwbar       = "c",
    weapon_crow_bar       = "c",

    weapon_combine_epearl = "z",

    --cw2.0 stray weapons
    cw_mr96 = "$",
}

local WeaponIconsCS = {
    weapon_usp          = "a",
    weapon_ak47         = "b",
    weapon_glock        = "c",
    weapon_tmp          = "d",
    weapon_aug          = "e",
    weapon_deagle       = "f",
    weapon_flashbang    = "g",
    weapon_hegrenade    = "h",
    weapon_g3sg1        = "i",
    weapon_knife        = "j",
    weapon_m3           = "k",
    weapon_mac10        = "l",
    weapon_p90          = "m",
    weapon_scout        = "n",
    weapon_sg550        = "o",
    weapon_smokegrenade = "p",
    weapon_ump45        = "q",
    weapon_awp          = "r",
    weapon_elite        = "s",
    weapon_famas        = "t",
    weapon_fiveseven    = "u",
    weapon_galil        = "v",
    weapon_m4a1         = "w",
    weapon_mp5navy      = "x",
    weapon_p228         = "y",
    weapon_m249         = "z",
    weapon_sg552        = "A",
    weapon_xm1014       = "B",
    weapon_c4           = "C",

    lite_usp          = "a",
    lite_ak47         = "b",
    lite_glock        = "c",
    lite_tmp          = "d",
    lite_aug          = "e",
    lite_deagle       = "f",
    lite_flashbang    = "g",
    lite_hegrenade    = "h",
    lite_g3sg1        = "i",
    lite_knife        = "j",
    lite_m3           = "k",
    lite_mac10        = "l",
    lite_p90          = "m",
    lite_scout        = "n",
    lite_sg550        = "o",
    lite_smokegrenade = "p",
    lite_ump          = "q",
    lite_awp          = "r",
    lite_dualberettas = "s",
    lite_famas        = "t",
    lite_fiveseven    = "u",
    lite_galil        = "v",
    lite_m4a1         = "w",
    lite_mp5          = "x",
    lite_p228         = "y",
    lite_m249         = "z",
    lite_sg552        = "A",
    lite_xm1014       = "B",
}

local WeaponIconsCSD = {
    cw_ak74                   = "b",
    cw_ar15                   = "w",
    cw_extrema_ratio_official = "j",
    cw_flash_grenade          = "P",
    cw_fiveseven              = "u",
    cw_scarh                  = "i",
    cw_frag_grenade           = "O",
    cw_g3a3                   = "i",
    cw_g36c                   = "i",
    cw_ump45                  = "q",
    cw_mp5                    = "x",
    cw_deagle                 = "f",
    cw_l115                   = "r",
    cw_l85a2                  = "i",
    cw_m14                    = "n",
    cw_m1911                  = "f",
    cw_m249_official          = "z",
    cw_m3super90              = "k",
    cw_mac11                  = "l",
    cw_p99                    = "a",
    cw_makarov                = "f",
    cw_shorty                 = "k",
    cw_smoke_grenade          = "Q",
    cw_vss                    = "i",
}

local WIOffsets = {
    hands              = 10,
    none               = 10,
    weapon_medkit      = 5,
    weapon_slap        = 5,
    weapon_alyxgun     = -5,
    weapon_annabelle   = 0,
    weapon_handgrenade = 10,
    cw_mr96            = 10,
    weapon_alyxgunr    = -5,
    weapon_annabeller  = 0,
    weapon_cyn_rr      = 10,
    weapon_archerxbow  = 10,
    weapon_coilgun     = 10,
    weapon_plasmanade  = 10,
    weapon_scrapper    = 25,
    oc_grapple         = -5,
}

local HL1Weps = {
    weapon_hl1_357         = true,
    weapon_hl1_glock       = true,
    weapon_hl1_crossbow    = true,
    weapon_hl1_crowbar     = true,
    weapon_hl1_egon        = true,
    weapon_hl1_handgrenade = true,
    weapon_hl1_hornetgun   = true,
    weapon_hl1_mp5         = true,
    weapon_hl1_rpg         = true,
    weapon_hl1_satchel     = true,
    weapon_hl1_shotgun     = true,
    weapon_hl1_snark       = true,
    weapon_hl1_gauss       = true,
    weapon_hl1_tripmine    = true,
}

local function DrawFontIcon(wep, x, y, w, h, tbl, font1, font2, offset)
    local class = wep:GetClass()

    y = y + (WIOffsets[class] or offset)
    x = x + 10
    w = w - 20

    local icon = tbl[class]

    draw_DrawText(icon, font2, x + w / 2, y, colText, TEXT_ALIGN_CENTER)
    draw_DrawText(icon, font1, x + w / 2, y, colText, TEXT_ALIGN_CENTER)
end

local function DWSHL1(wep, x, y, wide, tall, alpha)
    surface_SetDrawColor(colText.r, colText.g, colText.b, alpha)
    surface_SetTexture(wep.WepSelectIcon)

    y = y + 30
    x = x + 10
    wide = wide - 20

    surface_DrawTexturedRect(x, y, wide, wide / 2)
end

local iconCache = {}
local defaultIcon = surface_GetTextureID("weapons/swep")

local function DWSFallback(wep, x, y, wide, tall, alpha)
    local class = wep:GetClass()

    if not iconCache[class] then
        if Material("vgui/entities/" .. class):IsError() then
            iconCache[class] = defaultIcon
        else
            iconCache[class] = surface_GetTextureID("vgui/entities/" .. class)
        end
    end

    surface_SetDrawColor(255, 255, 255, alpha)
    if isnumber(wep.WepSelectIcon) then
        surface_SetTexture(wep.WepSelectIcon == defaultIcon and iconCache[class] or (wep.WepSelectIcon or defaultIcon))
    elseif type(wep.WepSelectIcon) == "IMaterial" then
        surface_SetMaterial(wep.WepSelectIcon)
    else
        surface_SetTexture(defaultIcon)
    end

    y = y + 10
    x = x + 10
    wide = wide - 20

    surface_DrawTexturedRect(x, y, wide, wide / 2)
    if isfunction(wep.PrintWeaponInfo) and wep.DrawWeaponInfoBox == true then
        wep:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
    end
end

local DWSCustom = {}

local function DrawWeaponBox(x, y, wep, selected, settings)
    if not IsValid(wep) then return end
    local class = wep:GetClass()

    local w, h = ScreenScaleH(settings.WeaponWidth), selected and ScreenScaleH(settings.WeaponHeightSelected) or ScreenScaleH(settings.WeaponHeight)

    draw_RoundedBox(10, x, y, w, h, colBack)

    local textY = ScreenScaleH(settings.WeaponHeight) / 2
    if selected then
        textY = ScreenScaleH(settings.TextYPos)
    elseif settings.WeaponHeight > settings.TextYPos then
        textY = ScreenScaleH(settings.TextYPos)
    end

    draw_DrawText(CHUD.WeaponNames[class] or (wep.PrintName or language_GetPhrase(class)), "CHUD-HUDSelectionText", x + w / 2, y + textY, colText, TEXT_ALIGN_CENTER)

    if wep.BounceWeaponIcon then wep.BounceWeaponIcon = false end

    if selected then
        local _y = y - ScreenScaleH(settings.IconOffset or 0)
        if WeaponIconsHL2[class] then
            DrawFontIcon(wep, x, _y, w, h, WeaponIconsHL2, "CHUD-WeaponSelect-HL2", "CHUD-WeaponSelected-HL2", 24)
        elseif WeaponIconsCS[class] then
            DrawFontIcon(wep, x, _y, w, h, WeaponIconsCS, "CHUD-WeaponSelect-CS", "CHUD-WeaponSelected-CS", 30)
        elseif WeaponIconsCSD[class] then
            DrawFontIcon(wep, x, _y, w, h, WeaponIconsCSD, "CHUD-WeaponSelect-CSD", "CHUD-WeaponSelected-CSD", 60)
        elseif HL1Weps[class] then
            DWSHL1(wep, x, _y, w, h, 255)
        elseif DWSCustom[class] then
            DWSCustom[class](x, _y, w, h, 255)
        else
            if wep.DrawWeaponSelection and isfunction(wep.DrawWeaponSelection) then
                if wep.IsSWCSWeapon then
                    surface_SetDrawColor(colText.r, colText.g, colText.b, 255)
                else
                    surface_SetDrawColor(255, 255, 255, 255)
                end
                wep:DrawWeaponSelection(x, y, w, h, 255)
            else
                DWSFallback(wep, x, y, w, h, 255)
            end
        end
    end

    return h
end

local function DrawSlot(x, y, slot, tCacheLength, settings)
    if (tCacheLength and tCacheLength[slot] == 0) and not settings.HideEmptySlots then
        draw_RoundedBox(10, x, y, ScreenScaleH(settings.BoxSize), ScreenScaleH(settings.BoxSize), colBack)
    end
    if tCacheLength and tCacheLength[slot] ~= 0 then
        draw_RoundedBox(10, x, y, ScreenScaleH(settings.BoxSize), ScreenScaleH(settings.BoxSize), colBack)
        draw_DrawText(slot, "CHUD-HUDSelectionNumbers", x + ScreenScaleH(settings.SelectionNumberPosX), y + ScreenScaleH(settings.SelectionNumberPosY), colText)
    end
end

local slots = {
    [1] = function(x, y, w, spacing, boxSize, tCacheLength, settings)
        for i = 0, 4 do
            DrawSlot(x + w + spacing + ((spacing + boxSize) * i), y, i + 2, tCacheLength, settings)
        end
    end,
    [2] = function(x, y, w, spacing, boxSize, tCacheLength, settings)
        DrawSlot(x, y, 1, tCacheLength, settings)
        for i = 0, 3 do
            DrawSlot(x + spacing + boxSize + w + spacing + ((spacing + boxSize) * i), y, i + 3, tCacheLength, settings)
        end
    end,
    [3] = function(x, y, w, spacing, boxSize, tCacheLength, settings)
        DrawSlot(x, y, 1, tCacheLength, settings)
        DrawSlot(x + (spacing + boxSize), y, 2, tCacheLength, settings)
        for i = 0, 2 do
            DrawSlot(x + (spacing + boxSize) * 2 + w + spacing + ((spacing + boxSize) * i), y, i + 4, tCacheLength, settings)
        end
    end,
    [4] = function(x, y, w, spacing, boxSize, tCacheLength, settings)
        for i = 0, 2 do
            DrawSlot(x + (spacing + boxSize) * i, y, i + 1, tCacheLength, settings)
        end
        for i = 0, 1 do
            DrawSlot(x + (spacing + boxSize) * 3 + w + spacing + ((spacing + boxSize) * i), y, i + 5, tCacheLength, settings)
        end
    end,
    [5] = function(x, y, w, spacing, boxSize, tCacheLength, settings)
        for i = 0, 3 do
            DrawSlot(x + (spacing + boxSize) * i, y, i + 1, tCacheLength, settings)
        end
        DrawSlot(x + (spacing + boxSize) * 4 + w + spacing, y, 6, tCacheLength, settings)
    end,
    [6] = function(x, y, w, spacing, boxSize, tCacheLength, settings)
        for i = 0, 4 do
            DrawSlot(x + (spacing + boxSize) * i, y, i + 1, tCacheLength, settings)
        end
    end
}

local function DrawWeaponSelector(x, y, iCurSlot, iCurPos, tCache, tCacheLength, settings)
    colText = CHUD:GetHUDColor()
    colBack = CHUD:GetBackgroundColor()

    local spacing = ScreenScaleH(settings.BoxGap)
    local w = ScreenScaleH(settings.WeaponWidth)
    local boxSize = ScreenScaleH(settings.BoxSize)
    local wbw = boxSize + spacing

    for n, wslot in ipairs(tCache) do
        if n == iCurSlot and iCurPos > 0 then
            local x, y = x, y
            for c, wep in ipairs(wslot) do
                local h = DrawWeaponBox(x + wbw * (iCurSlot - 1), y, wep, c == iCurPos, settings) or 0
                if c == 1 then
                    draw_DrawText(iCurSlot, "CHUD-HUDSelectionNumbers", x + wbw * (iCurSlot - 1) + ScreenScaleH(settings.SelectionNumberPosX), y + ScreenScaleH(settings.SelectionNumberPosY), colText)
                end
                y = y + h + spacing
            end
        end
    end

    slots[iCurSlot](x, y, w, spacing, boxSize, tCacheLength, settings)
end

function CHUD:DrawLegacyWeaponSwitcher(flSelectTime, MAX_SLOTS, iCurSlot, iCurPos, tCache, tCacheLength, settings)
    local spacing = ScreenScaleH(settings.BoxGap)
    local w = ScreenScaleH(settings.WeaponWidth)
    local boxSize = ScreenScaleH(settings.BoxSize)
    local wbw = boxSize + spacing

    DrawWeaponSelector(self.CurResW / 2 - ((wbw * 5) + w) / 2, ScreenScaleH(16), iCurSlot, iCurPos, tCache, tCacheLength, settings)
end

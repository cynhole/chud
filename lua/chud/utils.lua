CHUD = CHUD or {}

function CHUD:HTag(str)
    return "CHUD." .. str
end

CHUD.Hooks = {}
function CHUD:AddHook(type, name, func)
    CHUD.Hooks[type] = CHUD.Hooks[type] or {}
end

local lply = LocalPlayer()
function CHUD:GetLocalPlayer()
    if not IsValid(lply) then
        lply = LocalPlayer()
    end

    return lply
end

function CHUD:DrawCircle(x, y, radius, seg, poly)
    local cir

    if poly and (poly.prevX ~= x or poly.prevY ~= y) or not poly then
        radius = radius * 0.5

        cir = {}

        table.insert(cir, { x = x, y = y, u = 0.5, v = 0.5 })
        for i = 0, seg do
            local a = math.rad((i / seg) * -360)
            table.insert(cir, { x = x + math.sin(a) * radius, y = y + math.cos(a) * radius, u = math.sin(a) * 0.5 + 0.5, v = math.cos(a) * 0.5 + 0.5 })
        end

        local a = math.rad(0)
        table.insert(cir, { x = x + math.sin(a) * radius, y = y + math.cos(a) * radius, u = math.sin(a) * 0.5 + 0.5, v = math.cos(a) * 0.5 + 0.5 })

        cir.prevX = x
        cir.prevY = y
    else
        cir = poly
    end

    surface.DrawPoly(cir)

    return cir
end

local function RGBToYCbCr(r, g, b)
    local Y  =  0.299 * r + 0.587 * g + 0.114 * b
    local Cb = -0.169 * r - 0.331 * g + 0.500 * b + 128
    local Cr =  0.500 * r - 0.419 * g - 0.081 * b + 128

    return Y, Cb, Cr
end
local function YCbCrToRGB(Y, Cb, Cr)
    local r = 1 * Y + 0 * (Cb - 128) + 1.4 * (Cr - 128)
    local g = 1 * Y - 0.343 * (Cb - 128) - 0.711 * (Cr - 128)
    local b = 1 * Y + 1.765 * (Cb - 128) + 0 * (Cr - 128)

    return r, g, b
end

local function Intersect(a, b, value)
    return a + (b - a) * value
end

function CHUD:IntersectColor(a, b, value)
    local Y1, Cb1, Cr1 = RGBToYCbCr(a.r,a.g,a.b)
    local Y2, Cb2, Cr2 = RGBToYCbCr(b.r,b.g,b.b)

    local r_, g_, b_ = Intersect(Y1, Y2, value), Intersect(Cb1, Cb2, value), Intersect(Cr1, Cr2, value)

    local R, G, B = YCbCrToRGB(r_, g_, b_)
    return Color(R, G, B)
end

function CHUD:StringToColor(str)
    local col = Color(255, 255, 255)

    local r, g, b = str:match("(%d+) (%d+) (%d+)")

    col.r = tonumber(r) or 255
    col.g = tonumber(g) or 255
    col.b = tonumber(b) or 255

    return col
end

function CHUD:GetBool(cvar)
    if not self.CVars[cvar] then return end

    return self.CVars[cvar]:GetBool()
end

function CHUD:GetColor(cvar)
    if not self.CVars[cvar] then return end

    return self:StringToColor(self.CVars[cvar]:GetString())
end

function CHUD:GetInt(cvar)
    if not self.CVars[cvar] then return end

    return self.CVars[cvar]:GetInt()
end

function CHUD:GetFloat(cvar)
    if not self.CVars[cvar] then return end

    return self.CVars[cvar]:GetFloat()
end

function CHUD:GetString(cvar)
    if not self.CVars[cvar] then return end

    return self.CVars[cvar]:GetString()
end

local externalVectorCache = {}
function CHUD:GetExternalVectorColor(cvar)
    if externalVectorCache[cvar] then
        return Vector(externalVectorCache[cvar]:GetString()):ToColor()
    end

    externalVectorCache[cvar] = GetConVar(cvar)
    return Vector(externalVectorCache[cvar]:GetString()):ToColor()
end

local teamColorCache = {}
local lastTeamCache = 0
local function RecacheTeams()
    for index, data in pairs(team.GetAllTeams()) do
        teamColorCache[index] = data.Color
    end
    lastTeamCache = RealTime()
end
RecacheTeams()

function CHUD:GetTeamColor(n)
    if not teamColorCache[n] then
        RecacheTeams()
    end
    if lastTeamCache + 5 < RealTime() then
        RecacheTeams()
    end

    return teamColorCache[n]
end

local floor = math.floor
local function _HSVToRGB(hue, sat, val)
    if sat == 0 then
        return val, val, val
    end

    hue = hue % 360

    local sector = floor(hue / 60)
    local sectorOffset = (hue / 60) - sector

    local c = val * (1 - sat)
    local x = val * (1 - sat * sectorOffset)
    local m = val * (1 - sat * (1 - sectorOffset))

    if sector == 0 then
        return val, m, c
    elseif sector == 1 then
        return x, val, c
    elseif sector == 2 then
        return c, val, m
    elseif sector == 3 then
        return c, x, val
    elseif sector == 4 then
        return m, c, val
    elseif sector == 5 then
        return val, c, x
    end
end

local function HSVToRGB(h, s, v)
    local r, g, b = _HSVToRGB(h, s, v)
    return r * 255, g * 255, b * 255
end

local hsvColor = Color(255, 255, 255, 255)
local function FastHSVToColor(h, s, v)
    hsvColor.r, hsvColor.g, hsvColor.b = HSVToRGB(h, s, v)
    return hsvColor
end

function CHUD:GetRainbow(speed, sat, val)
    return FastHSVToColor(RealTime() * self:GetInt(speed) % 360, math.Clamp(self:GetFloat(sat), 0, 1), math.Clamp(self:GetFloat(val), 0, 1))
end

function CHUD:GetHUDColor()
    if self:GetBool("UsePlayerColor") then
        return self:GetExternalVectorColor("cl_playercolor")
    elseif self:GetBool("UseWeaponColor") then
        return self:GetExternalVectorColor("cl_weaponcolor")
    elseif self:GetBool("UseTeamColor") then
        return self:GetTeamColor(self:GetLocalPlayer():Team())
    elseif self:GetBool("Rainbow") then
        return self:GetRainbow("RainbowSpeed", "RainbowSat", "RainbowValue")
    else
        return self:GetColor("CustomColor")
    end
end

function CHUD:GetDamageColor()
    if self:GetBool("UsePlayerColorDMG") then
        return self:GetExternalVectorColor("cl_playercolor")
    elseif self:GetBool("UseWeaponColorDMG") then
        return self:GetExternalVectorColor("cl_weaponcolor")
    elseif self:GetBool("UseTeamColorDMG") then
        return self:GetTeamColor(self:GetLocalPlayer():Team())
    else
        return self:GetColor("DamageColor")
    end
end

function CHUD:GetBackgroundColor(useAlpha)
    useAlpha = useAlpha == nil and true or useAlpha

    local alpha = self:GetInt("BackgroundAlpha")
    local color = self:GetColor("BackgroundColor")

    if self:GetBool("UsePlayerColorBG") then
        color = self:GetExternalVectorColor("cl_playercolor")
    elseif self:GetBool("UseWeaponColorBG") then
        color = self:GetExternalVectorColor("cl_weaponcolor")
    elseif self:GetBool("UseTeamColorBG") then
        color = self:GetTeamColor(self:GetLocalPlayer():Team())
    elseif self:GetBool("RainbowBG") then
        color = self:GetRainbow("RainbowSpeedBG", "RainbowSatBG", "RainbowValueBG")
    end

    return useAlpha == true and ColorAlpha(color, alpha) or color
end

function CHUD:GetTextSize(font, str)
    font = font or "DermaDefault"
    str = str or ""

    surface.SetFont(font)
    return surface.GetTextSize(str)
end

function CHUD:ShouldDrawHealth()
    local ply = self:GetLocalPlayer()

    if not IsValid(ply) then return false end

    local hideAtFull = self:GetBool("HideHPAtFull")
    local health = ply:Health()
    local maxHealth = ply:GetMaxHealth()

    if health <= 0 then
        return false
    elseif health == maxHealth and hideAtFull == true then
        return false
    elseif health == maxHealth and hideAtFull == false then
        return true
    end

    return true
end

function CHUD:ShouldDrawArmor()
    local ply = self:GetLocalPlayer()

    if not IsValid(ply) then return false end

    local hideAtFull = self:GetBool("HideArmorAtFull")
    local armor = ply.Armor and ply:Armor() or 0
    local maxArmor = armor > 100 and math.max(200, ply:GetMaxArmor()) or ply:GetMaxArmor()

    if armor <= 0 then
        return false
    elseif armor == maxArmor and hideAtFull == true then
        return false
    elseif armor == maxArmor and hideAtFull == false then
        return true
    end

    return true
end

function CHUD:GetPlayerVelocity()
    local lply = self:GetLocalPlayer()
    local v = lply:GetVelocity()

    if IsValid(lply:GetVehicle()) then
        if IsValid(lply:GetVehicle():GetParent()) then
            v = lply:GetVehicle():GetParent():GetVelocity()
        else
            v = lply:GetVehicle():GetVelocity()
        end
    end

    return v
end

local color_black = Color(0, 0, 0)
local color_white = Color(255, 255, 255)
function CHUD:GetReadableColor(col)
    return (col.r * 0.299 + col.g * 0.587 + col.b * 0.114) > 186 and color_black or color_white
end

CHUD.WeaponNames = {
    weapon_smg1          = language.GetPhrase("#HL2_SMG1"),
    weapon_shotgun       = language.GetPhrase("#HL2_Shotgun"),
    weapon_crowbar       = language.GetPhrase("#HL2_Crowbar"),
    weapon_pistol        = language.GetPhrase("#HL2_Pistol"),
    weapon_357           = language.GetPhrase("#HL2_357Handgun"),
    weapon_crossbow      = language.GetPhrase("#HL2_Crossbow"),
    weapon_physgun       = language.GetPhrase("#GMOD_Physgun"),
    weapon_rpg           = language.GetPhrase("#HL2_RPG"),
    weapon_bugbait       = language.GetPhrase("#HL2_Bugbait"),
    weapon_frag          = language.GetPhrase("#HL2_Grenade"),
    weapon_ar2           = language.GetPhrase("#HL2_Pulse_Rifle"),
    weapon_physcannon    = language.GetPhrase("#HL2_GravityGun"),
    weapon_stunstick     = language.GetPhrase("#HL2_StunBaton"),
    weapon_slam          = language.GetPhrase("#HL2_SLAM"),
    weapon_annabelle     = language.GetPhrase("#HL2_Annabelle"),
    weapon_handgrenade   = language.GetPhrase("#HL1_HandGrenade"),
    gmod_camera          = language.GetPhrase("#GMOD_Camera"),
    weapon_cubemap       = "Cubemap",
    none                 = "Hands",
}

-- legacy
CHUD.Utils = {}

function CHUD.Utils.DrawCircle(x, y, radius, seg, poly)
    return CHUD:DrawCircle(x, y, radius, seg, poly)
end

function CHUD.Utils.StrToCol(str)
end

function CHUD.Utils.IntersectColor(a, b, value)
    return CHUD:IntersectColor(a, b, value)
end

function CHUD.Utils.GetHUDColor()
    return CHUD:GetHUDColor()
end

function CHUD.Utils.GetDamageColor()
    return CHUD:GetDamageColor()
end

function CHUD.Utils.GetCrosshairColor()
    return CHUD:GetColor("CrosshairColor")
end

function CHUD.Utils.GetBGColor()
    return CHUD:GetBackgroundColor()
end

function CHUD.Utils.GetBGColorNoAlpha()
    return CHUD:GetBackgroundColor(false)
end

function CHUD.Utils.GetTextSize(font, str)
    return CHUD:GetTextSize(font, str)
end

function CHUD.Utils.ShouldDrawHealth()
    return CHUD:ShouldDrawHealth()
end

function CHUD.Utils.ShouldDrawArmor()
    return CHUD:ShouldDrawArmor()
end
local CHUD = CHUD

local LocalPlayer = LocalPlayer
local FrameTime = FrameTime
local Lerp = Lerp
local ColorAlpha = ColorAlpha
local Vector = Vector
local Material = Material

local mathClamp = math.Clamp
local mathPow = math.pow
local mathMin = math.min

local surfaceSetDrawColor = surface.SetDrawColor
local surfaceDrawOutlinedRect = surface.DrawOutlinedRect
local surfaceDrawRect = surface.DrawRect
local surfaceSetMaterial = surface.SetMaterial
local surfaceDrawTexturedRect = surface.DrawTexturedRect

local drawNoTexture = draw.NoTexture

local GTSC = CHUD.Utils.GetTextSize
local GetHUDColor = CHUD.Utils.GetHUDColor
local GetCrosshairColor = CHUD.Utils.GetCrosshairColor

local lply = LocalPlayer()

local function CalcMovement(ent)
    if not IsValid(ent) then return end

    local movementRecoil = ent.MovementRecoil or 0
    ent.MovementRecoil = ent.MovementRecoil or 0

    local vel = ent:GetVelocity()
    local len = vel:Length()
    local target = len / ent:GetWalkSpeed()
    local scale = 100
    if len > 0 then
        scale = 20
    end
    movementRecoil = Lerp(FrameTime() * scale, movementRecoil, target)
    movementRecoil = mathClamp(movementRecoil, 0, 2)

    ent.MovementRecoil = movementRecoil
end

hook.Add("CreateMove", "CHUD.CalcMovement", function()
    if not IsValid(lply) then lply = LocalPlayer() return end
    CalcMovement(lply)
end)

local xhairAlpha = CHUD.CVars.CrosshairAlpha
local xhairStyle = CHUD.CVars.CrosshairStyle
local xhairUseHUD = CHUD.CVars.CrosshairUseHUD
local xhairOutline = CHUD.CVars.CrosshairOutline
local xhairDynamic = CHUD.CVars.CrosshairDynamic
local xhairLength = CHUD.CVars.CrosshairLength
local xhairSize = CHUD.CVars.CrosshairSize
local xhairSpace = CHUD.CVars.CrosshairSpace
local function DrawDynamicCrosshair(x, y)
    if not IsValid(lply) then lply = LocalPlayer() return end
    local wep = lply:GetActiveWeapon()
    if not IsValid(wep) then return end
    local IsTFA = IsValid(wep) and wep.IsTFA and wep:IsTFA() or wep.IsTFAWeapon

    local size = xhairLength:GetInt()
    local width = xhairSize:GetInt()
    local space = xhairSpace:GetInt()
    local color = xhairUseHUD:GetBool() and GetHUDColor() or GetCrosshairColor()
    local alpha = xhairAlpha:GetInt()
    local dynamic = xhairDynamic:GetBool()
    local outline = xhairOutline:GetBool()

    if IsTFA then
        alpha = xhairAlpha:GetInt() * mathPow(mathMin(1 - ((wep.IronSightsProgress and not wep.DrawCrosshairIS) and wep.IronSightsProgress or 0), 1 - (wep.SprintProgress or 0), 1 - (wep.InspectingProgress or 0), wep.clrelp or 0), 2)
    end

    if wep.crosshairVisible and LerpCW20 then
        if not wep:crosshairVisible() then
            alpha = LerpCW20(FrameTime() * 15, alpha, 0)
        else
            alpha = LerpCW20(FrameTime() * 15, alpha, 255)
        end
    end

    local movementRecoil = 0
    if dynamic and IsValid(lply) then
        movementRecoil = lply.MovementRecoil or 0
        local gap = 15 * movementRecoil
        space = space + gap
    end

    local sizeH = size / 2

    if outline then
        surfaceSetDrawColor(0, 0, 0, alpha)
        surfaceDrawOutlinedRect(x - (width / 2) - 1, y - sizeH - space - 1, width + 2, sizeH + 2) --top
        surfaceDrawOutlinedRect(x - sizeH - space - 1, y - (width / 2) - 1, sizeH + 2, width + 2) --left
        surfaceDrawOutlinedRect(x - (width / 2) - 1, y + space - 1, width + 2, sizeH + 2) --bottom
        surfaceDrawOutlinedRect(x + space - 1, y - (width / 2) - 1, sizeH + 2, width + 2) --right
    end

    surfaceSetDrawColor(ColorAlpha(color, alpha))
    surfaceDrawRect(x - (width / 2), y - sizeH - space, width, sizeH) --top
    surfaceDrawRect(x - sizeH - space, y - (width / 2), sizeH, width) --left
    surfaceDrawRect(x - (width / 2), y + space, width, sizeH) --bottom
    surfaceDrawRect(x + space, y - (width / 2), sizeH, width) --right
end

local x1 = Material("sprites/hud/v_crosshair1")
local x2 = Material("sprites/hud/v_crosshair2")
local xw1, xh1 = x1:Width(), x1:Height()
local xw2, xh2 = x2:Width(), x2:Height()

local circle
local shadowCircle
local drawCircle = CHUD.Utils.DrawCircle
local alpha = xhairAlpha:GetInt()
local function DrawCrosshair(x, y)
    if not IsValid(lply) then lply = LocalPlayer() return end
    if not lply:Alive() then return end
    local wep = lply:GetActiveWeapon()
    if not IsValid(wep) then return end
    if wep.DrawCrosshair == false then return end

    local IsTFA = IsValid(wep) and (wep.IsTFA and wep:IsTFA() or wep.IsTFAWeapon) or false

    if IsTFA then
        alpha = xhairAlpha:GetInt() * mathPow(mathMin(1 - ((wep.IronSightsProgress and not wep.DrawCrosshairIS) and wep.IronSightsProgress or 0), 1 - (wep.SprintProgress or 0), 1 - (wep.InspectingProgress or 0), wep.clrelp or 0), 2)
    elseif wep.crosshairVisible and LerpCW20 then
        if not wep:crosshairVisible() then
            alpha = LerpCW20(FrameTime() * 15, alpha, 0)
        else
            alpha = LerpCW20(FrameTime() * 15, alpha, xhairAlpha:GetInt())
        end
    else
        alpha = xhairAlpha:GetInt()
    end

    local style = xhairStyle:GetInt()
    local color = ColorAlpha(xhairUseHUD:GetBool() and GetHUDColor() or GetCrosshairColor(), alpha)

    if style == -1 then
        return
    elseif style == 1 then
        drawNoTexture()
        if xhairOutline:GetBool() then
            surfaceSetDrawColor(ColorAlpha(Color(0, 0, 0, 192), alpha))
            shadowCircle = drawCircle(x, y, 7, 8, shadowCircle)
        end

        surfaceSetDrawColor(color)
        circle = drawCircle(x, y, 5, 8, circle)
    elseif style == 2 then
        surfaceSetDrawColor(color)
        surfaceSetMaterial(x1)
        surfaceDrawTexturedRect(x - xw1 / 2, y - xh1 / 2, xw1, xh1)
    elseif style == 3 then
        surfaceSetDrawColor(color)
        surfaceSetMaterial(x2)
        surfaceDrawTexturedRect(x - xw2 / 2, y - xh2 / 2, xw2, xh2)
    elseif style == 4 then
        DrawDynamicCrosshair(x, y)
    else
        local w, h = GTSC("CHUD-HL2-Crosshairs", "Q")
        draw.DrawText("Q", "CHUD-HL2-Crosshairs", x, y - h / 2, color, TEXT_ALIGN_CENTER)
    end
end
CHUD.DrawCrosshair = DrawCrosshair

local DisableExtras = {
    CHudZoom                  = true,
    CHudSquadStatus           = true,
    CHudPoisonDamageIndicator = true,
    CHudGeiger                = true,
    CHudDamageIndicator       = true,
}

local ToHide = {
    CHudHealth        = true,
    CHudBattery       = true,
    CHudAmmo          = true,
    CHudSecondaryAmmo = true,
    CHudVehicle       = true,
}

local drawCrosshair = true
local enabled = CHUD.CVars.Enabled
local dbg = CHUD.CVars.Debug
local no_extras = CHUD.CVars.DisableExtras
local xhair_override = CHUD.CVars.CrosshairOverride
local xhair_tfa = CHUD.CVars.CrosshairTFA
hook.Add("HUDShouldDraw", "CHUD", function(name)
    if not IsValid(lply) then lply = LocalPlayer() end
    if dbg:GetBool() then return true end

    if DisableExtras[name] and no_extras:GetBool() then return false end
    if ToHide[name] and enabled:GetBool() then return false end

    local wep = IsValid(lply) and lply:GetActiveWeapon() or NULL

    if name == "CHudCrosshair" then
        if not enabled:GetBool() then return true end
        if xhairStyle:GetInt() == -1 then return true end
        if IsValid(wep) and wep:IsWeapon() then
            if isfunction(wep.DoDrawCrosshair) then
                local IsTFA = IsValid(wep) and wep.IsTFA and wep:IsTFA() or wep.IsTFAWeapon
                if xhair_override:GetBool() then
                    drawCrosshair = true
                    return
                end
                if IsTFA and xhair_tfa:GetBool() then
                    drawCrosshair = true
                else
                    drawCrosshair = lply:ShouldDrawLocalPlayer()
                end

                -- gross hack, reduces perf a bit but won't visually draw crosshairs twice
                render.SetScissorRect(0, 0, 1, 1, true)
                local hide = wep:DoDrawCrosshair()
                render.SetScissorRect(0, 0, 0, 0, false)
                drawCrosshair = not hide

                if hide == false then
                    return false
                end

                return
            end

            if not wep.DoDrawCrosshair and wep.ShouldDrawCrosshair then
                local should = wep:ShouldDrawCrosshair()
                drawCrosshair = should ~= nil and should or true
                return
            end

            if wep.DrawCrosshair == false then
                drawCrosshair = false
                return
            end

        end

        drawCrosshair = true
        return false
    end
end)

local cl_drawhud = GetConVar("cl_drawhud")
local curstyle = CHUD.CVars.Style
hook.Add("HUDPaint", "CHUD", function()
    if not IsValid(lply) then lply = LocalPlayer() end
    if not cl_drawhud:GetBool() then return end
    if not enabled:GetBool() then return end

    local style = CHUD.Styles[curstyle:GetString()]

    if not style or not style.Draw then
        style = CHUD.Styles["hl2"]
    end

    if not CHUD.Styles["hl2"] then
        include("chud/chud.lua")
        return
    end

    if style.Draw and isfunction(style.Draw) then
        style.Draw()
    end
end)

hook.Add("HUDPaint", "CHUD.Crosshair", function()
    if not IsValid(lply) then lply = LocalPlayer() end
    if not cl_drawhud:GetBool() then return end
    if not enabled:GetBool() then return end
    if drawCrosshair then
        if lply:ShouldDrawLocalPlayer() and not lply:InVehicle() then
            local tr = lply:GetEyeTrace()
            local pos = Vector(tr.HitPos):ToScreen()
            DrawCrosshair(pos.x, pos.y)
        else
            DrawCrosshair(CHUD.CurResW / 2, CHUD.CurResH / 2)
        end
    end
end)

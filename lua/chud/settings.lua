local draw, surface, math, string, LocalPlayer, ScreenScaleH = draw, surface, math, string, LocalPlayer, ScreenScaleH
local GTSC = CHUD.Utils.GetTextSize

local COLOR_BLACK = Color(0, 0, 0)
local COLOR_WHITE = Color(255, 255, 255)

local function Main(panel)
    local conf = {
        Options = {},
        CVars = {},
        Label = "#Presets",
        MenuButton = "1",
        Folder = "chud_main"
    }

    conf.Options["#Default"] = {
        chud_enabled        = "1",
        chud_wpn_enabled    = "1",
        chud_showvelocity   = "0",
        chud_disable_extras = "1",
        chud_hidehpatfull   = "1",
        chud_negativehp     = "0",
        chud_altammo_oldpos = "1",
        chud_additivefonts  = "1",
    }

    panel:AddControl("ComboBox", conf)

    local lbl = vgui.Create("DLabel", panel)
    lbl:Dock(TOP)
    lbl:SetFont("DermaLarge")
    lbl:SetText("Main")
    lbl:SizeToContents()
    lbl:DockMargin(8, 16, 8, 0)
    lbl:SetDark(true)

    panel:CheckBox("Enable CHUD", "chud_enabled")

    panel:CheckBox("Enable Custom Weapon Switcher", "chud_wpn_enabled")

    local lbl = vgui.Create("DLabel", panel)
    lbl:Dock(TOP)
    lbl:SetFont("DermaLarge")
    lbl:SetText("Features")
    lbl:SizeToContents()
    lbl:DockMargin(8, 16, 8, 0)
    lbl:SetDark(true)

    panel:CheckBox("Show Velocity", "chud_showvelocity")

    panel:CheckBox("Disable extra, unused/annoying HUD elements", "chud_disable_extras")
    panel:ControlHelp("Disables zoom overlay, squad status, poison indicator, geiger clicking sound and side damage indicators")

    panel:CheckBox("Hide Health at full HP", "chud_hidehpatfull")

    panel:CheckBox("Show Negative HP Values", "chud_negativehp")

    panel:CheckBox("Use Stock Secondary Ammo Position", "chud_altammo_oldpos")
    panel:ControlHelp("Disabled: Secondary Ammo displayed above Primary Ammo")

    panel:CheckBox("Additive Fonts", "chud_additivefonts")
    panel:ControlHelp("Disabled: Fonts become solid apposed to transparent. Accuracy lost in return for visibility.")
end

local function Colors(panel)
    local styleName = CHUD:GetString("Style")
    local wpnName = CHUD:GetString("StyleWeapon")

    local conf = {
        Options = {},
        CVars = {},
        Label = "#Presets",
        MenuButton = "1",
        Folder = "chud_colors"
    }

    conf.Options["#Default"] = {
        chud_customcolor     = "255 220 0",
        chud_damagecolor     = "255 0 0",
        chud_useplayercolor  = "0",
        chud_useweaponcolor  = "0",
        chud_useteamcolor    = "0",

        chud_backgroundcolor   = "0 0 0",
        chud_backgroundalpha   = "80",
        chud_useplayercolor_bg = "0",
        chud_useweaponcolor_bg = "0",
        chud_useteamcolor_bg   = "0",

        chud_rainbow    = "0",
        chud_rbow_speed = "2",
        chud_rbow_sat   = "1",
        chud_rbow_value = "1",

        chud_rainbow_bg    = "0",
        chud_rbow_speed_bg = "2",
        chud_rbow_sat_bg   = "1",
        chud_rbow_value_bg = "1",
    }

    panel:AddControl("ComboBox", conf)

    local preview = vgui.Create("EditablePanel", panel)
    preview:Dock(TOP)
    preview:SetTall(ScreenScaleH(36) + 32)
    preview:DockMargin(8, 8, 8, 8)
    local x = 0
    function preview:Paint(w, h)
        x = x + 0.25
        draw.RoundedBox(0, x,           0, w, h, COLOR_BLACK)
        draw.RoundedBox(0, x - w,       0, w, h, COLOR_WHITE)
        draw.RoundedBox(0, x - (w * 2), 0, w, h, COLOR_BLACK)

        local style = CHUD.Styles[styleName]

        if not style or not style.Draw then
            style = CHUD.Styles["hl2"]
        end

        if not CHUD.Styles["hl2"] then
            return
        end

        style.Demo(100, 16, 16)

        if x > w * 2 then x = 0 end
    end

    local preview2 = vgui.Create("EditablePanel", panel)
    preview2:Dock(TOP)
    preview2:SetTall(ScreenScaleH(36) + 32)
    preview2:DockMargin(8, 8, 8, 8)
    function preview2:Paint(w, h)
        x = x + 0.25
        draw.RoundedBox(0, x,           0, w, h, COLOR_BLACK)
        draw.RoundedBox(0, x - w,       0, w, h, COLOR_WHITE)
        draw.RoundedBox(0, x - (w * 2), 0, w, h, COLOR_BLACK)
        local style = CHUD.Styles[styleName]

        if not style or not style.Draw then
            style = CHUD.Styles["hl2"]
        end

        if not CHUD.Styles["hl2"] then
            return
        end

        style.Demo(25, 16, 16)

        if x > w * 2 then x = 0 end
    end

    local stylePnl = vgui.Create("EditablePanel", panel)
    stylePnl:Dock(TOP)
    stylePnl:SetTall(24)
    stylePnl:DockMargin(8, 8, 8 , 8)

    local slbl = stylePnl:Add("DLabel")
    slbl:Dock(LEFT)
    slbl:DockMargin(4, 4, 36, 4)
    slbl:SetText("HUD Style")
    slbl:SizeToContents()
    slbl:SetDark(true)

    local styles = stylePnl:Add("DComboBox")
    styles:Dock(FILL)
    styles:SetValue(CHUD.Styles[styleName] and CHUD.Styles[styleName].Name or CHUD.Styles["hl2"].Name)

    for id, data in pairs(CHUD.Styles) do
        if not data.Draw or not data.Demo or not isfunction(data.Draw) or not isfunction(data.Demo) then continue end
        styles:AddChoice(data.Name, id, id == styleName)
    end

    styles.OnSelect = function(s, i, name, value)
        CHUD.CVars.Style:SetString(value)
        styleName = value
    end

    local wpnPnl = vgui.Create("EditablePanel", panel)
    wpnPnl:Dock(TOP)
    wpnPnl:SetTall(24)
    wpnPnl:DockMargin(8, 8, 8, 8)

    local wlbl = wpnPnl:Add("DLabel")
    wlbl:Dock(LEFT)
    wlbl:DockMargin(4, 4, 36, 4)
    wlbl:SetText("Weapon Switcher")
    wlbl:SizeToContents()
    wlbl:SetDark(true)

    local wpnStyle = wpnPnl:Add("DComboBox")
    wpnStyle:Dock(FILL)
    wpnStyle:SetValue(CHUD.Styles[wpnName] and CHUD.Styles[wpnName].Name or CHUD.Styles["hl2"].Name)

    for id, data in pairs(CHUD.Styles) do
        if not data.HasWeaponSwitcher then continue end
        wpnStyle:AddChoice(data.Name, id, id == wpnName)
    end

    wpnStyle.OnSelect = function(s, i, name, value)
        CHUD.CVars.StyleWeapon:SetString(value)
        wpnName = value
    end

    panel:Help("\nForeground/Text Color:")

    local mixer_text = vgui.Create("DColorMixer", panel)
    mixer_text:Dock(TOP)
    mixer_text:SetTall(245)
    mixer_text:DockMargin(8, 8, 8, 8)
    mixer_text:SetAlphaBar(false)
    mixer_text:SetColor(CHUD:GetHUDColor())
    function mixer_text:ValueChanged()
        CHUD.CVars.CustomColor:SetString(Format("%d %d %d", self:GetColor():Unpack()))
    end

    panel:CheckBox("Use Player Color", "chud_useplayercolor")
    panel:CheckBox("Use Weapon Color", "chud_useweaponcolor")
    panel:CheckBox("Use Team Color", "chud_useteamcolor")

    panel:Help("\nDamaged/Low Ammo Color:")

    local mixer_dmg = vgui.Create("DColorMixer", panel)
    mixer_dmg:Dock(TOP)
    mixer_dmg:SetTall(245)
    mixer_dmg:DockMargin(8, 8, 8, 8)
    mixer_dmg:SetAlphaBar(false)
    mixer_dmg:SetColor(CHUD:GetDamageColor())
    function mixer_dmg:ValueChanged()
        CHUD.CVars.DamageColor:SetString(Format("%d %d %d", self:GetColor():Unpack()))
    end

    panel:CheckBox("Use Player Color", "chud_useplayercolor_dmg")
    panel:CheckBox("Use Weapon Color", "chud_useweaponcolor_dmg")
    panel:CheckBox("Use Team Color", "chud_useteamcolor_dmg")

    panel:Help("\nBackground Color:")

    local mixer_back = vgui.Create("DColorMixer", panel)
    mixer_back:Dock(TOP)
    mixer_back:SetTall(245)
    mixer_back:DockMargin(8, 8, 8, 8)
    mixer_back:SetAlphaBar(false)
    mixer_back:SetColor(CHUD:GetBackgroundColor(false))
    function mixer_back:ValueChanged()
        CHUD.CVars.BackgroundColor:SetString(Format("%d %d %d", self:GetColor():Unpack()))
    end

    panel:NumSlider("Alpha", "chud_backgroundalpha", 0, 255, 0)
    panel:CheckBox("Use Player Color", "chud_useplayercolor_bg")
    panel:CheckBox("Use Weapon Color", "chud_useweaponcolor_bg")
    panel:CheckBox("Use Team Color", "chud_useteamcolor_bg")

    panel:Help("\nRainbow Settings:")

    panel:CheckBox("Enable for Foreground", "chud_rainbow")
    panel:NumSlider("FG: Speed", "chud_rbow_speed", 1, 1000, 0)
    panel:NumSlider("FG: Saturation", "chud_rbow_sat", 0, 1, 2)
    panel:NumSlider("FG: Value", "chud_rbow_value", 0, 1, 2)

    panel:CheckBox("Enable for Background", "chud_rainbow_bg")
    panel:NumSlider("BG: Speed", "chud_rbow_speed_bg", 1, 1000, 0)
    panel:NumSlider("BG: Saturation", "chud_rbow_sat_bg", 0, 1, 2)
    panel:NumSlider("BG: Value", "chud_rbow_value_bg", 0, 1, 2)
end

-- Crosshair --

-- Subtract 2
local crosshairs = {
    [1] = "Disabled (Engine Crosshair)",
    [2] = "HL2 Default",
    [3] = "Dot",
    [4] = "Vehicle",
    [5] = "Vehicle Alt",
    [6] = "Dynamic",
}
local function Crosshair(panel)
    local conf = {
        Options = {},
        CVars = {},
        Label = "#Presets",
        MenuButton = "1",
        Folder = "chud_crosshair"
    }

    conf.Options["#Default"] = {
        chud_crosshair         = "0",
        chud_crosshair_size    = "2",
        chud_crosshair_length  = "8",
        chud_crosshair_space   = "4",
        chud_crosshair_outline = "1",
        chud_crosshair_dynamic = "1",
        chud_crosshair_color   = "255 255 255",
        chud_crosshair_alpha   = "255",
        chud_crosshair_usehud  = "1",
    }

    panel:AddControl("ComboBox", conf)

    local preview = vgui.Create("EditablePanel", panel)
    preview:Dock(TOP)
    preview:SetTall(ScreenScaleH(20)+64)
    preview:DockMargin(8, 8, 8, 8)
    local x = 0
    function preview:Paint(w, h)
        x = x + 0.5
        draw.RoundedBox(0, x,           0, w, h, COLOR_BLACK)
        draw.RoundedBox(0, x - w,       0, w, h, COLOR_WHITE)
        draw.RoundedBox(0, x - (w * 2), 0, w, h, COLOR_BLACK)

        CHUD.DrawCrosshair(w / 2, h / 2)

        if x > w * 2 then x = 0 end
    end

    local comboPnl = vgui.Create("EditablePanel", panel)
    comboPnl:Dock(TOP)
    comboPnl:SetTall(24)
    comboPnl:DockMargin(8, 8, 8, 8)

    local clabel = comboPnl:Add("DLabel")
    clabel:Dock(LEFT)
    clabel:DockMargin(4, 4, 36, 4)
    clabel:SetText("Crosshair Style")
    clabel:SizeToContents()
    clabel:SetDark(true)

    local combo = comboPnl:Add("DComboBox")
    combo:Dock(FILL)
    combo:SetValue(crosshairs[CHUD.CVars.CrosshairStyle:GetInt() - 2] or crosshairs[1])

    for value, name in pairs(crosshairs) do
        combo:AddChoice(name, value, CHUD.CVars.CrosshairStyle:GetInt() == value - 2)
    end

    combo.OnSelect = function(s, i, name, value)
        CHUD.CVars.CrosshairStyle:SetInt(value - 2)
    end

    panel:Help("Crosshair Color:")

    local mixer = vgui.Create("DColorMixer", panel)
    mixer:Dock(TOP)
    mixer:SetTall(245)
    mixer:DockMargin(8, 8, 8, 8)
    mixer:SetAlphaBar(false)
    mixer:SetColor(CHUD.Utils.GetCrosshairColor())
    function mixer:ValueChanged()
        local color = self:GetColor()
        RunConsoleCommand("chud_crosshair_color", ("%d %d %d"):format(color.r, color.g, color.b))
    end

    panel:NumSlider("Alpha", "chud_crosshair_alpha", 0, 255, 0)
    panel:CheckBox("Use HUD Color", "chud_crosshair_usehud")

    panel:Help("\nDynamic Crosshair Settings:")

    panel:CheckBox("Movement Based Spacing", "chud_crosshair_dynamic")
    panel:CheckBox("Outline", "chud_crosshair_outline")
    panel:NumSlider("Length", "chud_crosshair_length", 0, 128, 0)
    panel:NumSlider("Width", "chud_crosshair_size", 0, 64, 0)
    panel:NumSlider("Spacing", "chud_crosshair_space", 0, 24, 0)
end

hook.Add("PopulateToolMenu", "CHUD", function()
	spawnmenu.AddToolMenuOption("Utilities", "CHUD", "CHUD1", "Configuration", "", "", Main)
	spawnmenu.AddToolMenuOption("Utilities", "CHUD", "CHUD2", "Colors & Style", "", "", Colors)
    spawnmenu.AddToolMenuOption("Utilities", "CHUD", "CHUD3", "Crosshair", "", "", Crosshair)
end)

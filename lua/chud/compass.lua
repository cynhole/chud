local LocalPlayer = LocalPlayer
local IsValid = IsValid
local draw_SimpleText = draw.SimpleText
local draw_RoundedBox = draw.RoundedBox
local surface_SetAlphaMultiplier = surface.SetAlphaMultiplier
local surface_SetDrawColor = surface.SetDrawColor
local surface_DrawRect = surface.DrawRect
local render_SetScissorRect = render.SetScissorRect
local math_Clamp = math.Clamp
local math_sin = math.sin
local math_abs = math.abs
local dec2rad = math.rad
local Remap = math.Remap
local AngleDifference = math.AngleDifference
local rshift = bit.rshift

local ScreenScaleH = CHUD.Utils.ScreenScaleH
local GTSC = CHUD.Utils.GetTextSize
local GetBGColor = CHUD.Utils.GetBGColor
local GetHUDColor = CHUD.Utils.GetHUDColor
local GetDamageColor = CHUD.Utils.GetDamageColor

local lply = LocalPlayer()

local hud_locator_alpha = CreateClientConVar("hud_locator_alpha", 230):GetInt()
local hud_locator_fov = CreateClientConVar("hud_locator_fov", 230):GetFloat()

cvars.AddChangeCallback("hud_locator_alpha", function(_,_,new)
    hud_locator_alpha = new
end)
cvars.AddChangeCallback("hud_locator_fov", function(_,_,new)
    hud_locator_fov = new
end)

local enabled = CHUD.CVars.Compass:GetBool()
local isTop = CHUD.CVars.CompassTop:GetBool()
local multiplier = CHUD.CVars.CompassMultiplier:GetFloat()
local useDmgColor = CHUD.CVars.CompassUseDmg:GetBool()
local labels = CHUD.CVars.CompassLabels:GetBool()
local labelsBottom = CHUD.CVars.CompassLabelsBottom:GetBool()
local useSubcardnals = CHUD.CVars.CompassSubCardnals:GetBool()
local NUM_GRADUATIONS = CHUD.CVars.CompassTicks:GetInt()

cvars.AddChangeCallback("chud_compass", function(_,_,new)
    enabled = tobool(new)
end)
cvars.AddChangeCallback("chud_compass_top", function(_,_,new)
    isTop = tobool(new)
end)
cvars.AddChangeCallback("chud_compass_multiplier", function(_,_,new)
    multiplier = new
end)
cvars.AddChangeCallback("chud_compass_dmg_col", function(_,_,new)
    useDmgColor = tobool(new)
end)
cvars.AddChangeCallback("chud_compass_labels", function(_,_,new)
    labels = tobool(new)
end)
cvars.AddChangeCallback("chud_compass_labels_bottom", function(_,_,new)
    labelsBottom = tobool(new)
end)
cvars.AddChangeCallback("chud_compass_subcardnals", function(_,_,new)
    useSubcardnals = tobool(new)
end)
cvars.AddChangeCallback("chud_compass_ticks", function(_,_,new)
    NUM_GRADUATIONS = new
end)

local c_w, c_h = 64, 24

local function LocatorXPositionForYawDiff(w, yawDiff)
    local fov = hud_locator_fov / 2
    local remappedAngle = Remap( yawDiff, -fov, fov, -90, 90 )
    local cosine = math_sin(dec2rad(remappedAngle))
    local element_wide = w

    local position = rshift(element_wide, 1) + (rshift(element_wide, 1) * cosine)

    return position
end

local cardnals = {
    ["90"] = "N",
    ["-90"] = "S",
    ["0"] = "E",
    ["180"] = "W",
    ["-180"] = "W",
}

local subcardnals = {
    ["-45"] = "SE",
    ["45"] = "NE",
    ["135"] = "NW",
    ["-135"] = "SW",
}

local function DrawGraduations(x, y, yaw)
    local w, h = ScreenScaleH(c_w * math_Clamp(math_abs(multiplier), 1, 3)), ScreenScaleH(c_h)

    local icon_wide, icon_tall
    local xPos, yPos
    local fov = hud_locator_fov / 2

    local element_tall = h

    local col = GetHUDColor()

    local angleStep = 360 / math_Clamp(NUM_GRADUATIONS, 4, 360)
    local tallLine = true
    local majorAxis = true

    for angle = -180, 180, angleStep do
        yPos = rshift(element_tall, 1)

        if angle == 90 and useDmgColor then
            col = GetDamageColor()
        else
            col = GetHUDColor()
        end

        if angle == 0 or angle == 180 or angle == -180 or angle == 90 or angle == -90 then
            majorAxis = true
        else
            majorAxis = false
        end

        if tallLine then
            icon_wide, icon_tall = 2, majorAxis and 12 or 6
            tallLine = false
        else
            icon_wide, icon_tall = 2, majorAxis and 12 or 2
            tallLine = true
        end

        local flDiff = AngleDifference(yaw, angle)

        if math_abs(flDiff) > fov then continue end

        local xPosition = LocatorXPositionForYawDiff(w, flDiff)
        xPos = xPosition
        xPos = xPos - rshift(icon_wide, 1)

        if (angle == 0 or angle == 180 or angle == -180 or angle == 90 or angle == -90) and labels then
            local tw,th = GTSC("CHUD-HL2-HUDDefault",cardnals[tostring(angle)] or "")
            draw_SimpleText(cardnals[tostring(angle)] or "", "CHUD-HL2-HUDDefault", x + xPos, y + yPos - (labelsBottom and -(th / 2) or (th / 2)) - (labelsBottom and -(icon_tall / 2) or (icon_tall / 2)), col, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
        end

        if (angle == 45 or angle == 135 or angle == -135 or angle == -45) and labels and useSubcardnals then
            local tw,th = GTSC("CHUD-HL2-HUDDefault",subcardnals[tostring(angle)] or "")
            draw_SimpleText(subcardnals[tostring(angle)] or "", "CHUD-HL2-HUDDefault", x + xPos, y + yPos - (labelsBottom and -(th / 2) or (th / 2)) - (labelsBottom and -(icon_tall / 2) or (icon_tall / 2)), col, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
        end
        surface_SetDrawColor(col)
        surface_DrawRect(x + xPos, y + yPos - (icon_tall / 2), icon_wide, icon_tall)
    end
end

local function DrawCompass(x, y)
    if not IsValid(lply) then lply = LocalPlayer() return end
    local w,h = ScreenScaleH(c_w * math_Clamp(multiplier, 1, 3)), ScreenScaleH(c_h)

    render_SetScissorRect(x, y, x + w, y + h, true)
    draw_RoundedBox(4, x, y, w, h, GetBGColor())

    local alpha = hud_locator_alpha
    surface_SetAlphaMultiplier(alpha / 255)

    local flYawPlayerForward = lply:EyeAngles().y

    DrawGraduations(x, y, flYawPlayerForward)

    surface_SetAlphaMultiplier(1)
    render_SetScissorRect(0, 0, 0, 0, false)
end

local chud_enabled = CHUD.CVars.Enabled
local cl_drawhud = GetConVar("cl_drawhud")
hook.Add("HUDPaint", "CHUD.Compass", function()
    if not IsValid(lply) then lply = LocalPlayer() end
    if not cl_drawhud:GetBool() then return end
    if not chud_enabled:GetBool() then return end
    if enabled then
        local x, y = (CHUD.CurResW / 2) - ScreenScaleH((c_w * math_Clamp(multiplier, 1, 3)) / 2), isTop and c_h or CHUD.CurResH - ScreenScaleH(36)

        DrawCompass(x, y)
    end
end)
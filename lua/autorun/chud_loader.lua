if SERVER then
    for _, f in ipairs(file.Find("chud/*.lua", "LUA")) do
        AddCSLuaFile("chud/" .. f)
    end
    for _, f in ipairs(file.Find("chud/styles/*.lua", "LUA")) do
        AddCSLuaFile("chud/styles/" .. f)
    end
    for _, f in ipairs(file.Find("chud/weapon_switcher/*.lua", "LUA")) do
        AddCSLuaFile("chud/weapon_switcher/" .. f)
    end
end

if CLIENT and engine.ActiveGamemode() == "sandbox" then
    include("chud/chud.lua")
end